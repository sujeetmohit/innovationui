import { AppConfirmModule } from './app-confirm/app-confirm.module';
import { ResultMessageComponent } from './reusables/result-message/result-message.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AppLoaderModule } from './app-loader/app-loader.module';


const classesToInclude = [
  ResultMessageComponent
]

@NgModule({
  imports: [
    CommonModule,
    AppLoaderModule,
    AppConfirmModule
  ],
  providers: [
  ],
  declarations: classesToInclude,
  exports: classesToInclude
})
export class SharedModule { }
