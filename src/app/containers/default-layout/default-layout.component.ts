import { Component } from "@angular/core";
import { INavData } from "@coreui/angular";
import { Router } from "@angular/router";
import { LoginService } from "../../Services/Login Service/login.service";
import { RolesManagementService } from '../../Services/Roles Service/roles-management.service';
import * as _ from 'lodash';
import { GlobalVariables } from '../../Services/utility/globalVariables';


@Component({
  selector: "app-dashboard",
  templateUrl: "./default-layout.component.html",
  styleUrls: ["./default-layout.component.css"],
})
export class DefaultLayoutComponent {
  public sidebarMinimized = false;
  public navItem = [
    {
      name: 'Dashboard',
      url: '/dashboard',
      icon: 'icon-speedometer',
      roles: ['manager', 'hr manager', 'recruiter', 'hr', 'associate', 'interviewer', 'finance', 'admin'],
      priority: 1
    },
    {
      name: 'Idea',
      url: '/Idea',
      icon: 'icon-basket',
      roles: ['manager', 'hr manager', 'recruiter', 'hr', 'associate', 'interviewer', 'admin'],
      priority: 2,
      children: [
        {
          name: 'Create New',
          url: '/Idea/newIdeaSubmission',
          icon: 'icon-arrow-right',
          roles: ['manager', 'hr manager', 'recruiter', 'hr', 'associate', 'interviewer', 'admin'],
        },
        {
          name: 'Submitted',
          url: '/Idea/submittedIdeaSubmission',
          icon: 'icon-arrow-right',
          roles: ['manager', 'hr manager', 'recruiter', 'hr', 'associate'],
        },
      ]
    },

    {
      name: 'ProjectCharter',
      url: '/ProjectCharter',
      icon: 'icon-basket',
      roles: ['manager', 'hr manager', 'recruiter', 'hr', 'associate', 'interviewer', 'admin'],
      priority: 2,
      children: [
        // {
        //   name: 'New',
        //   url: '/ProjectCharter/createProjectCharter',
        //   icon: 'icon-arrow-right',
        //   roles: ['manager', 'hr manager', 'recruiter', 'hr', 'associate', 'interviewer', 'admin'],
        // },
        {
          name: 'Submitted',
          url: '/ProjectCharter/submittedProjectCharter',
          icon: 'icon-arrow-right',
          roles: ['manager', 'hr manager', 'recruiter', 'hr', 'associate'],
        },

      ]
    },

  ];
  public newList: INavData[] = [];
  public outputNavList: INavData[] = [];
  public tempList: INavData[] = [];

  public Userroles = new Array();
  public flag = false;
  public userEmail: string = '';
  public userName: string = '';
  public empId: string = '';

  constructor(private router: Router,
    private loginService: LoginService,
    private rolesManagementService: RolesManagementService,
    private globalService: GlobalVariables) {

    this.userEmail = this.globalService.getUserEmail();
    this.userName = this.globalService.getUserName();
    this.empId = this.globalService.getEmpId();
    //get user roles list
    let arr = [];

    let stored_roles = this.globalService.getRoles();
    let obj_roles = JSON.parse(stored_roles);
    // let obj_roles = stored_roles;
    if (obj_roles != undefined && obj_roles != null) {
      for (let i = 0; i < obj_roles.length; i++) {
        arr.push(obj_roles[i].authority);
      }
    }


    for (let i = 0; i < arr.length; i++) {
      if (arr[i].toLowerCase() === "hr manager") {
        Object.assign(this.tempList, this.navItem.filter((x) => x.roles.includes("hr manager")));
      } else if (arr[i].toLowerCase() === "manager") {
        Object.assign(this.tempList, this.navItem.filter((x) => x.roles.includes("manager")));
      } else if (arr[i].toLowerCase() === "recruiter") {
        Object.assign(this.tempList, this.navItem.filter((x) => x.roles.includes("recruiter")));
      } else if (arr[i].toLowerCase() === "hr") {
        Object.assign(this.tempList, this.navItem.filter((x) => x.roles.includes("hr")));
      } else if (arr[i].toLowerCase() === "associate") {
        Object.assign(this.tempList, this.navItem.filter((x) => x.roles.includes("associate")));
      } else if (arr[i].toLowerCase() === "interviewer") {
        Object.assign(this.tempList, this.navItem.filter((x) => x.roles.includes("interviewer")));
      } else if (arr[i].toLowerCase() === "finance") {
        Object.assign(this.tempList, this.navItem.filter((x) => x.roles.includes("finance")));
      } else if (arr[i].toLowerCase() === "admin") {
        Object.assign(this.tempList, this.navItem.filter((x) => x.roles.includes("admin")));
      }

      for (let i = 0; i < this.tempList.length; i++) {
        if (this.newList.map(x => x.name != this.tempList[i].name)) {
          this.newList.push(this.tempList[i]);
        }
      }
    }

    this.outputNavList = this.newList.filter((n, i) => this.newList.indexOf(n) === i);

    // for (let i = 1; i < this.outputNavList.length; i++) {
    //   for (let j = 0; j < this.outputNavList[i].children.length; j++) {
    //     this.flag = false;
    //     for (let l = 0; l < arr.length; l++) {
    //       if (this.outputNavList[i].children[j].roles.includes(arr[l].toLowerCase())) {
    //         this.flag = true;
    //       }
    //     }

    //     if (!this.flag) {
    //       this.outputNavList[i].children.splice(j, 1);
    //       j -= 1;
    //     }
    //   }
    // }

    // this.outputNavList = this.outputNavList.sort((n1, n2) => {
    //   if (n1.priority > n2.priority) {
    //     return 1;
    //   }

    //   if (n1.priority < n2.priority) {
    //     return -1;
    //   }

    //   return 0;
    // });

  }

  toggleMinimize(e) {
    this.sidebarMinimized = e;
  }

  applicationLogout() {
    this.loginService.rolesList.pop();
    this.rolesManagementService.userRoles.pop();
    this.router.navigate(["../login"]);
  }

}
