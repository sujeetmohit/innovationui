import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Component, Inject, Output, EventEmitter, Injector, OnInit } from '@angular/core';

import { FormBuilder, FormGroup, Validators, FormControl, FormGroupDirective } from '@angular/forms';
import Utils from '../../Services/utility/utils';

@Component({
  selector: 'app-confirm',
  templateUrl: './app-confirm.component.html'
})
export class AppComfirmComponent implements OnInit  {

  
  
  

  constructor(
    public dialogRef: MatDialogRef<AppComfirmComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any, public injector: Injector
  ) {
   
  }

  ngOnInit() {
   
  }

 

  emitInput() {
    // if () {
      // this.dialogRef.close(this.rejectReason);
    // } else {
      this.dialogRef.close(true);
    // }
 }

}
