import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { Observable } from 'rxjs/Observable';

import { Injectable } from '@angular/core';

import { AppComfirmComponent } from './app-confirm.component';

interface confirmData {
  title?: string,
  message?: string,
  button?: string,
  delete?: boolean
}

@Injectable()
export class AppConfirmService {
  public dialogRef: MatDialogRef<AppComfirmComponent>;
  constructor(private dialog: MatDialog) { }

  public confirm(data:confirmData = {}): Observable<boolean> {
    data.title = data.title || 'Confirm';
    data.message = data.message || 'Are you sure?';
    data.button = data.button || 'OK';
    data.delete = data.delete || false;
   // this.dialogRef: MatDialogRef<AppComfirmComponent>;
    this.dialogRef = this.dialog.open(AppComfirmComponent, {
      width: 'auto',
      height: 'auto',
      panelClass: 'custom-modalbox',
      disableClose: true,
      data: { title: data.title, message: data.message, button: data.button, delete: data.delete }
    });
    return this.dialogRef.afterClosed();
  }

  emitEvent() {
    this.dialogRef.afterClosed().subscribe(value => {
    //  alert(value);
    });
  }


}
