import { Component, Input, Output, EventEmitter, OnChanges, OnInit, OnDestroy } from '@angular/core';
import Utils from '../../../Services/utility/utils';



@Component({
  selector: 'app-reusables-result-message',
  templateUrl: './result-message.component.html',
  styleUrls: ['./result-message.component.scss']
})
export class ResultMessageComponent implements OnInit, OnChanges,OnDestroy {
  @Input() message: string = 'SUCCESS';
  @Input() type: string = ''; // Other types are SUCCESS, ERROR, INFO, WARNING
  @Input() showCloseButton: boolean = false;
  @Input() allowAutoClose: boolean = false;
  @Input() autoCloseTime: number = 5000;
  @Input() allowHTML: boolean = false;
  @Output() valueChange = new EventEmitter<boolean>();


  public messageType: string = 'success';
  public isHTMLContent: boolean = false;

  constructor() {
// window.scroll(0,0);
  }

  ngOnInit() {
    //DO NOT MOVE - The below has to be on Init only
    Utils.log("Inside ngOnInit " + this.type);
    this.messageType = this.type.toLocaleLowerCase();
    if (this.type == "SUCCESS") {
      this.showCloseButton = false;
      this.allowAutoClose = true;
    } else if (this.type == "ERROR") {
      this.showCloseButton = true;
      this.allowAutoClose = false;
    }
    this.isHTMLContent = this.allowHTML;
    this.onTextChanged();
  }

  ngOnChanges() {
    Utils.log("ngonchanges called");
    window.scroll(0,0);
    this.onTextChanged();
  }

  onTextChanged() {
    if (this.allowAutoClose == true) {
      if (Utils.isValidInput(this.message)) {
        setTimeout(() => {
          Utils.log("Clear Message called in Set timeout");
          this.clearMessage();
        }, this.autoCloseTime);
      }
    }
  }
  
ngOnDestroy() {

}

clearMessage() {
  this.message = "";
  this.valueChange.emit(true);

  }

isValidMessage() {
   return Utils.isValidInput(this.message);
  }
}
