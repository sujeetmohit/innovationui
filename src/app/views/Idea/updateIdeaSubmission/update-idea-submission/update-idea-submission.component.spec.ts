import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateIdeaSubmissionComponent } from './update-idea-submission.component';

describe('UpdateIdeaSubmissionComponent', () => {
  let component: UpdateIdeaSubmissionComponent;
  let fixture: ComponentFixture<UpdateIdeaSubmissionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateIdeaSubmissionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateIdeaSubmissionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
