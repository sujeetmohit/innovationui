import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { RolesManagementService } from '../../../../Services/Roles Service/roles-management.service';
import { AppLoaderService } from '../../../../containers/app-loader/app-loader.service';
import { GlobalVariables } from '../../../../Services/utility/globalVariables';
import * as _ from 'lodash';
import { IdeaSubmissionEntity } from '../../../../Modals/Idea/IdeaSubmission';
import { IdeaSubmission } from '../../../../Services/IdeaSubmission/ideasubmissionservice';

@Component({
  selector: 'app-update-idea-submission',
  templateUrl: './update-idea-submission.component.html',
  styleUrls: ['./update-idea-submission.component.css']
})
export class UpdateIdeaSubmissionComponent implements OnInit {

  @Input() max: any;

  today = new Date();
  allowedExtensions = ['doc', 'docx', 'pdf', 'msg', 'pst', 'edb', 'ost', 'eml', 'mbox'];
  maxSize = 5000000;
  ideaSubmissionId: string;
  updateIdeaSubmission: FormGroup;
  public errorMessageStatus = '';
  public successMessageStatus = '';
  submitted = false;
  leverTechnologyList = new Array();
  projectTypeList = new Array();
  public userEmail;
  public isAdmin = false;
  public isHrManager = false;
  showDownloadIdeaSubmissionButton = false;
  showUploadIdeaSubmissionButton = false;
  file: File = null;
  ideaFile: File = null;
  ideaFileName: String = ""
  locationId: number = 0;
  departmentId: number = 0;
  designationId: number = 0;
  hodName: string = "";

  constructor(
    private formBuilder: FormBuilder,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private rolesManagementService: RolesManagementService,
    private loader: AppLoaderService,
    private globalService: GlobalVariables,
    private ideaSubmission: IdeaSubmission,
    private ideaSubmissionEntityObj: IdeaSubmissionEntity,) {

    this.loader.open();
    this.today.setDate(this.today.getDate());
    this.ideaSubmissionId = atob(this.activatedRoute.snapshot.queryParams.rowId);
    this.userEmail = this.globalService.getUserEmail();
    this.isAdmin = this.rolesManagementService.isAdmin;
    this.isHrManager = this.rolesManagementService.isHrManager;

    //Lever Technology dropdown Onload
    this.ideaSubmission.getLeverTechnologyDetails().subscribe((data) => {
      this.leverTechnologyList = data["body"];
      this.onLeverTechnologySelect();
    });

    //Project Type dropdown Onload
    this.ideaSubmission.getProjectTypeDetails().subscribe((data) => {
      this.projectTypeList = data["body"];
      this.onProjectTypeSelect();
    });
  }

  ngOnInit() {
    this.successMessageStatus = this.globalService.getPhraseToShow();
    this.updateIdeaSubmission = this.formBuilder.group({
      Id: [""],
      ownerempid: [""],
      ideaownername: [""],
      designationname: [""],
      client: [""],
      departmentname: [""],
      hod: [""],
      locationname: [""],
      ideaproposalname: ["", Validators.required],
      problemstatement: ["", Validators.required],
      levertechnologyName: ["", Validators.required],
      leverid: ["", Validators.required],
      riskdescription: ["", Validators.required],
      benefits: ["", Validators.required],
      projectcommencementdate: ["", Validators.required],
      projectcompletiondate: ["", Validators.required],
      projecttypeName: ["", Validators.required],
      projecttypeid: ["", Validators.required],
      managerfeedback: ["", Validators.required],
      receiptfilename: [""],
    });

    this.disableControl();
    this.getIdeaSubmissionbyId();
  }

  getIdeaSubmissionbyId() {
   
    this.ideaSubmission.getIdeaSubmissionbyId(this.ideaSubmissionId).subscribe((response) => {
      if (response["body"] != null) {
        this.updateIdeaSubmission.patchValue(response["body"][0]);
        this.onLeverTechnologySelect();
        this.onProjectTypeSelect();
        if (response["body"][0].receiptfilename != "") {
          this.showDownloadIdeaSubmissionButton = true;
          this.showUploadIdeaSubmissionButton = false;
        }
      } else {
        this.router.navigate(["../submittedIdeaSubmission"], {
          relativeTo: this.activatedRoute,
        });
      }
      this.loader.close();
    }, (error) => {
      this.loader.close();
    });
  }

  onSubmit() {
   
    this.submitted = true;
    if (this.updateIdeaSubmission.invalid) {
      return;
    }
    
    this.successMessageStatus = '';
    this.loader.open();
    this.ideaSubmissionEntityObj.id = Number(this.ideaSubmissionId);
    this.ideaSubmissionEntityObj.ownerempid = this.updateIdeaSubmission.controls['ownerempid'].value;
    this.ideaSubmissionEntityObj.ideaownername = this.updateIdeaSubmission.controls['ideaownername'].value;
    this.ideaSubmissionEntityObj.designationname = this.updateIdeaSubmission.controls['designationname'].value;
    // this.ideaSubmissionEntityObj.designationid = this.updateIdeaSubmission.controls['designationid'].value;
    this.ideaSubmissionEntityObj.client = this.updateIdeaSubmission.controls['client'].value;
    // this.ideaSubmissionEntityObj.departmentid = this.updateIdeaSubmission.controls['departmentid'].value;
    this.ideaSubmissionEntityObj.departmentname = this.updateIdeaSubmission.controls['departmentname'].value;
    this.ideaSubmissionEntityObj.hod = this.updateIdeaSubmission.controls['hod'].value;
    // this.ideaSubmissionEntityObj.locationid = this.updateIdeaSubmission.controls['locationid'].value;
    this.ideaSubmissionEntityObj.locationname = this.updateIdeaSubmission.controls['locationname'].value;
    this.ideaSubmissionEntityObj.ideaproposalname = this.updateIdeaSubmission.controls['ideaproposalname'].value;
    this.ideaSubmissionEntityObj.problemstatement = this.updateIdeaSubmission.controls['problemstatement'].value;
    this.ideaSubmissionEntityObj.leverid = this.updateIdeaSubmission.controls['leverid'].value;
    this.ideaSubmissionEntityObj.levertechnologyName = this.updateIdeaSubmission.controls['levertechnologyName'].value;
    this.ideaSubmissionEntityObj.riskdescription = this.updateIdeaSubmission.controls['riskdescription'].value;
    this.ideaSubmissionEntityObj.benefits = this.updateIdeaSubmission.controls['benefits'].value;
    this.ideaSubmissionEntityObj.projectcommencementdate = this.updateIdeaSubmission.controls['projectcommencementdate'].value;
    this.ideaSubmissionEntityObj.projectcompletiondate = this.updateIdeaSubmission.controls['projectcompletiondate'].value;
    this.ideaSubmissionEntityObj.projecttypeid = this.updateIdeaSubmission.controls['projecttypeid'].value;
    this.ideaSubmissionEntityObj.projecttypeName = this.updateIdeaSubmission.controls['projecttypeName'].value;
    this.ideaSubmissionEntityObj.managerfeedback = this.updateIdeaSubmission.controls['managerfeedback'].value;
    this.ideaSubmissionEntityObj.receiptfilename = this.updateIdeaSubmission.controls['receiptfilename'].value;
    // this.ideaSubmissionEntityObj.createdby = this.updateIdeaSubmission.controls['expectedRelivingdate'].value;
    this.ideaSubmissionEntityObj.updateby = this.userEmail;
    this.ideaSubmission.UpdateIdeaSubmissionDetails(this.ideaSubmissionEntityObj).subscribe((reponse) => {
      // this.globalService.setPhraseToShow("Idea Submission Details Saved Successfully.");
      alert("Idea Submission Details Updated Successfully.");
      let responseBody: string = reponse["body"];
      let array: string[] = responseBody.split('= ');
      this.ideaSubmissionId = array[1];
      this.uploadIdeaSubmissionFileToServer(this.file);
      this.loader.close();
      this.submitted = false;
      this.updateIdeaSubmission.reset();

      this.router.navigate(["../submittedIdeaSubmission"], {
        relativeTo: this.activatedRoute,
      });
    }, (error) => {
      this.loader.close();
    });
    // this.ideaSubmission.UpdateIdeaSubmissionDetails(this.ideaSubmissionEntityObj).subscribe((reponse) => {
    //   // this.uploadIdeaSubmissionFileToServer(this.file);
    //   this.globalService.setPhraseToShow("Idea Submission Details Updated Successfully.");
    //   this.loader.close();
    //   this.submitted = false;
    //   this.updateIdeaSubmission.reset();
    //   this.router.navigate(["../submittedIdeaSubmission"], {
    //     relativeTo: this.activatedRoute,
    //   });
    // }, (error) => {
    //   this.loader.close();
    // });
  }

  onLeverTechnologySelect() {
    let leverList = this.leverTechnologyList;
    // let technologyName = _.find(leverList, { 'id': Number(this.updateIdeaSubmission.controls['leverid'].value) }).technologyName;
    // this.updateIdeaSubmission.controls['levertechnologyName'].setValue(technologyName);
  }

  onProjectTypeSelect() {
    let projectTypeList = this.projectTypeList;
    // let projectTypeName = _.find(projectTypeList, { 'id': Number(this.updateIdeaSubmission.controls['projecttypeid'].value) }).projecttypeName;
    // this.updateIdeaSubmission.controls['projecttypeName'].setValue(projectTypeName);
  }

  handleIdeaFileInput(ideaFile: FileList) {
    this.ideaFile = ideaFile.item(0);
  }

  uploadIdeaSubmissionFileToServer(file: File) {
    this.loader.open();
    this.ideaSubmission.uploadIdeaSubmissionFile(file, Number(this.ideaSubmissionId)).subscribe((response) => {
      this.loader.close();
    }, (error) => {
      this.loader.close();
    });
  }

  downloadIdeaSubmission() {
    window.location.href = this.ideaSubmission.baseUrl
      + "fileupload/ideaSubmission/downloadIdeaSubmission?ideaSubmissionId=" + this.ideaSubmissionId;
  }

  deleteIdeaSubmissionile() {
    if (confirm("Confirm to delete previous file?")) {
      this.ideaSubmission.deleteIdeaSubmissionFile(Number(this.ideaSubmissionId)).subscribe((data) => {
        if (data["status"] === true) {
          alert("Document deleted successfully..!");
        }
        this.showDownloadIdeaSubmissionButton = false;
        this.showUploadIdeaSubmissionButton = true;
      });
    }
  }

  selectIdeaSubmissionFile(event) {
    this.errorMessageStatus = '';
    this.file = event.target.files[0];
    this.ideaFileName = event.target.files[0].name;
    let file1 = event.target.files[0];
    if (file1 instanceof File) {
      if (this.allowedExtensions.indexOf(this.getExtension(file1.name).toLowerCase()) == -1) {
        alert('This file type is not allowed, allowed axtension is: ' + this.allowedExtensions);
        event.srcElement.value = null;
        this.ideaFileName = '';
      }
      else if (this.fileMaxSize(file1)) {
        alert('File size limit is exceeded, max file size is 5MB');
        this.errorMessageStatus = 'File size limit is exceeded, max file size is 5MB';
        event.srcElement.value = null;
        this.ideaFileName = '';
      }
    }
  }

  GoBack() {
    this.router.navigate(["../submittedIdeaSubmission"], {
      relativeTo: this.activatedRoute,
    });
  }

  private getExtension(filename: string): null | string {
    if (filename.indexOf('.') === -1) {
      return null;
    }
    return filename.split('.').pop();
  }
  fileMaxSize(file: File) {
    if (file instanceof File && file.size > this.maxSize) {
      return true;
    }
    else return false;;
  }

  getToday(): string {
    return new Date().toISOString().split('T')[0]
  }

  specialCharacterValidation(event) {
    var regex = /[a-zA-Z0-9-_. ]/;
    var input = String.fromCharCode(event.keyCode);
    if (regex.test(input)) {
      return true;
    } else {
      event.preventDefault();
      return false;
    }
  }

  disableControl() {
    this.updateIdeaSubmission.controls['ownerempid'].disable();
    this.updateIdeaSubmission.controls['ideaownername'].disable();
    this.updateIdeaSubmission.controls['designationname'].disable();
    this.updateIdeaSubmission.controls['client'].disable();
    this.updateIdeaSubmission.controls['departmentname'].disable();
    this.updateIdeaSubmission.controls['hod'].disable();
    this.updateIdeaSubmission.controls['locationname'].disable();
    this.updateIdeaSubmission.controls['managerfeedback'].disable();
  }

}
