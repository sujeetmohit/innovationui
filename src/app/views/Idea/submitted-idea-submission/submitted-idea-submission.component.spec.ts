import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubmittedIdeaSubmissionComponent } from './submitted-idea-submission.component';

describe('SubmittedIdeaSubmissionComponent', () => {
  let component: SubmittedIdeaSubmissionComponent;
  let fixture: ComponentFixture<SubmittedIdeaSubmissionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubmittedIdeaSubmissionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubmittedIdeaSubmissionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
