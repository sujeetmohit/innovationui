import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewIdeaSubmissionComponent } from './new-idea-submission.component';

describe('NewIdeaSubmissionComponent', () => {
  let component: NewIdeaSubmissionComponent;
  let fixture: ComponentFixture<NewIdeaSubmissionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewIdeaSubmissionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewIdeaSubmissionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
