import { DatePipe } from '@angular/common';
import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
// import { MomentDateAdapter } from '@angular/material-moment-adapter';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { HtmlEditorService, ImageService, LinkService, TableService, ToolbarService } from '@syncfusion/ej2-angular-richtexteditor';
import { Utils } from 'ngx-bootstrap/utils';
import { AppLoaderService } from '../../../containers/app-loader/app-loader.service';
import { GlobalVariables } from '../../../Services/utility/globalVariables';
import { IdeaSubmissionEntity } from '../../../Modals/Idea/IdeaSubmission';
import { RolesManagementService } from '../../../Services/Roles Service/roles-management.service';
import { IdeaSubmission } from '../../../Services/IdeaSubmission/ideasubmissionservice';

export const MY_DATE_FORMATS = {
  parse: {
    dateInput: 'DD/MM/YYYY',
  },
  display: {
    dateInput: 'DD/MM/YYYY',
    monthYearLabel: 'MMMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY'
  },
};

@Component({
  selector: 'app-new-idea-submission',
  templateUrl: './new-idea-submission.component.html',
  styleUrls: ['./new-idea-submission.component.css'],
  providers: [{ provide: MAT_DATE_LOCALE, useValue: "en-US" },
  // {
  //   provide: DateAdapter,
  //   useClass: MomentDateAdapter,
  //   deps: [MAT_DATE_LOCALE],
  // },
  { provide: MAT_DATE_FORMATS, useValue: MY_DATE_FORMATS },
    ToolbarService, LinkService, ImageService, HtmlEditorService, TableService],
})
export class NewIdeaSubmissionComponent implements OnInit {

  @Input() max: any;

  hiddenField = false;
  today = new Date();
  allowedExtensions = ['doc', 'docx', 'pdf', 'msg', 'pst', 'edb', 'ost', 'eml', 'mbox'];
  maxSize = 5000000;
  ideaSubmissionId: string;
  insertIdeaSubmission: FormGroup;
  public errorMessageStatus = '';
  public successMessageStatus = '';
  submitted = false;
  leverTechnologyList = new Array();
  projectTypeList = new Array();
  public userEmail;
  public isAdmin = false;
  public isHrManager = false;
  // showDownloadIdeaSubmissionButton = false;
  // showUploadIdeaSubmissionButton = false;
  file: File = null;
  ideaFile: File = null;
  ideaFileName: String = "";
  locationId: number = 0;
  departmentId: number = 0;
  designationId: number = 0;
  hodName: string = "";

  constructor(
    private formBuilder: FormBuilder,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private rolesManagementService: RolesManagementService,
    private loader: AppLoaderService,
    private globalService: GlobalVariables,
    private ideaSubmission: IdeaSubmission,
    private ideaSubmissionEntityObj: IdeaSubmissionEntity,) {

    this.loader.open();
    this.today.setDate(this.today.getDate());
    this.userEmail = this.globalService.getUserEmail();
    this.isAdmin = this.rolesManagementService.isAdmin;
    this.isHrManager = this.rolesManagementService.isHrManager;

    //Lever Technology dropdown Onload
    this.ideaSubmission.getLeverTechnologyDetails().subscribe((data) => {
      this.leverTechnologyList = data["body"];
      this.onLeverTechnologySelect();
    });

    //Project Type dropdown Onload
    this.ideaSubmission.getProjectTypeDetails().subscribe((data) => {
      this.projectTypeList = data["body"];
      this.onProjectTypeSelect();
    });
  }

  ngOnInit() {
    this.successMessageStatus = this.globalService.getPhraseToShow();
    this.insertIdeaSubmission = this.formBuilder.group({
      ownerempid: [""],
      ideaownername: [""],
      designationname: [""],
      client: [""],
      departmentname: [""],
      hod: [""],
      locationname: [""],
      ideaproposalname: ["", Validators.required],
      problemstatement: ["", Validators.required],
      levertechnologyName: [""],
      leverid: ["", Validators.required],
      riskdescription: ["", Validators.required],
      benefits: ["", Validators.required],
      projectcommencementdate: ["", Validators.required],
      projectcompletiondate: ["", Validators.required],
      projecttypeName: [""],
      projecttypeid: ["", Validators.required],
      managerfeedback: ["", Validators.required],
      receiptfilename: [""],
    });

    this.disableControl();
    this.getEmployeeDetails();
  }

  getEmployeeDetails() {
    this.ideaSubmission.getEmployeeDetailsByEmail(this.userEmail).subscribe((response) => {
      if (response["body"] != null) {
        this.insertIdeaSubmission.controls['ownerempid'].setValue(response["body"][0].employeeNo);
        this.insertIdeaSubmission.controls['ideaownername'].setValue(response["body"][0].fullName);
        this.insertIdeaSubmission.controls['designationname'].setValue(response["body"][0].designationName);
        this.insertIdeaSubmission.controls['client'].setValue(response["body"][0].clientName);
        this.insertIdeaSubmission.controls['departmentname'].setValue(response["body"][0].workdeptName);
        this.insertIdeaSubmission.controls['hod'].setValue(response["body"][0].reportingManager);
        this.insertIdeaSubmission.controls['locationname'].setValue(response["body"][0].locationName);
        this.locationId = response["body"][0].locationId;
        this.designationId = response["body"][0].designationId;
        this.departmentId = response["body"][0].workdeptId;
        this.hodName = response["body"][0].reportingManager;
        this.onLeverTechnologySelect();
        this.onProjectTypeSelect();
      } else {
        this.router.navigate(["../submittedIdeaSubmission"], {
          relativeTo: this.activatedRoute,
        });
      }
      this.loader.close();
    }, (error) => {
      this.loader.close();
    });
  }

  onSubmit() {
   
    this.submitted = true;

    if (this.insertIdeaSubmission.invalid) {
      return;
    }
    this.successMessageStatus = '';
    this.loader.open();
    this.ideaSubmissionEntityObj.ownerempid = this.insertIdeaSubmission.controls['ownerempid'].value;
    this.ideaSubmissionEntityObj.ideaownername = this.insertIdeaSubmission.controls['ideaownername'].value;
    this.ideaSubmissionEntityObj.ownerofficialemail = this.userEmail;
    this.ideaSubmissionEntityObj.ownerreportingmanageremail = this.insertIdeaSubmission.controls['hod'].value;
    this.ideaSubmissionEntityObj.designationname = this.insertIdeaSubmission.controls['designationname'].value;
    this.ideaSubmissionEntityObj.designationid = this.designationId;
    this.ideaSubmissionEntityObj.client = this.insertIdeaSubmission.controls['client'].value;
    this.ideaSubmissionEntityObj.departmentid = this.departmentId;
    this.ideaSubmissionEntityObj.departmentname = this.insertIdeaSubmission.controls['departmentname'].value;
    this.ideaSubmissionEntityObj.hod = this.insertIdeaSubmission.controls['hod'].value;
    this.ideaSubmissionEntityObj.reportingmanagerName = this.hodName;
    this.ideaSubmissionEntityObj.locationid = this.locationId;
    this.ideaSubmissionEntityObj.locationname = this.insertIdeaSubmission.controls['locationname'].value;
    this.ideaSubmissionEntityObj.ideaproposalname = this.insertIdeaSubmission.controls['ideaproposalname'].value;
    this.ideaSubmissionEntityObj.problemstatement = this.insertIdeaSubmission.controls['problemstatement'].value;
    this.ideaSubmissionEntityObj.leverid = this.insertIdeaSubmission.controls['leverid'].value;
    this.ideaSubmissionEntityObj.levertechnologyName = this.insertIdeaSubmission.controls['levertechnologyName'].value;
    this.ideaSubmissionEntityObj.riskdescription = this.insertIdeaSubmission.controls['riskdescription'].value;
    this.ideaSubmissionEntityObj.benefits = this.insertIdeaSubmission.controls['benefits'].value;
    this.ideaSubmissionEntityObj.projectcommencementdate = this.insertIdeaSubmission.controls['projectcommencementdate'].value;
    this.ideaSubmissionEntityObj.projectcompletiondate = this.insertIdeaSubmission.controls['projectcompletiondate'].value;
    this.ideaSubmissionEntityObj.projecttypeid = this.insertIdeaSubmission.controls['projecttypeid'].value;
    this.ideaSubmissionEntityObj.projecttypeName = this.insertIdeaSubmission.controls['projecttypeName'].value;
    this.ideaSubmissionEntityObj.managerfeedback = this.insertIdeaSubmission.controls['managerfeedback'].value;
    this.ideaSubmissionEntityObj.receiptfilename = this.insertIdeaSubmission.controls['receiptfilename'].value;
    this.ideaSubmissionEntityObj.createdby = this.userEmail;
    this.ideaSubmissionEntityObj.updateby = "";

    this.ideaSubmission.InsertUpdateIdeaSubmissionDetails(this.ideaSubmissionEntityObj).subscribe((reponse) => {
      // this.globalService.setPhraseToShow("Idea Submission Details Saved Successfully.");
      alert("Idea Submission Details Saved Successfully.");
      let responseBody: string = reponse["body"];
      let array: string[] = responseBody.split('= ');
      this.ideaSubmissionId = array[1];
      this.uploadIdeaSubmissionFileToServer(this.file);
      this.loader.close();
      this.submitted = false;
      this.insertIdeaSubmission.reset();

      this.router.navigate(["../submittedIdeaSubmission"], {
        relativeTo: this.activatedRoute,
      });
    }, (error) => {
      this.loader.close();
    });
  }

  onLeverTechnologySelect() {
    let leverList = this.leverTechnologyList;
    // let technologyName = _.find(leverList, { 'id': Number(this.insertIdeaSubmission.controls['leverid'].value) }).technologyName;
    // this.insertIdeaSubmission.controls['levertechnologyName'].setValue(technologyName);
  }

  onProjectTypeSelect() {
    let projectTypeList = this.projectTypeList;
    // let projectTypeName = _.find(projectTypeList, { 'id': Number(this.insertIdeaSubmission.controls['projecttypeid'].value) }).projecttypeName;
    // this.insertIdeaSubmission.controls['projecttypeName'].setValue(projectTypeName);
  }

  handleIdeaFileInput(ideaFile: FileList) {
    this.ideaFile = ideaFile.item(0);
  }

  uploadIdeaSubmissionFileToServer(file: File) {
    this.loader.open();
    this.ideaSubmission.uploadIdeaSubmissionFile(file, Number(this.ideaSubmissionId)).subscribe((response) => {
      this.loader.close();
    }, (error) => {
      this.loader.close();
    });
  }

  selectIdeaSubmissionFile(event) {
    this.errorMessageStatus = '';
    this.file = event.target.files[0];
    this.ideaFileName = event.target.files[0].name;
    let file1 = event.target.files[0];
    if (file1 instanceof File) {
      if (this.allowedExtensions.indexOf(this.getExtension(file1.name).toLowerCase()) == -1) {
        alert('This file type is not allowed, allowed axtension is: ' + this.allowedExtensions);
        event.srcElement.value = null;
        this.ideaFileName = '';
      }
      else if (this.fileMaxSize(file1)) {
        alert('File size limit is exceeded, max file size is 5MB');
        this.errorMessageStatus = 'File size limit is exceeded, max file size is 5MB';
        event.srcElement.value = null;
        this.ideaFileName = '';
      }
    }
  }

  GoBack() {
    this.router.navigate(["../submittedIdeaSubmission"], {
      relativeTo: this.activatedRoute,
    });
  }

  private getExtension(filename: string): null | string {
    if (filename.indexOf('.') === -1) {
      return null;
    }
    return filename.split('.').pop();
  }
  fileMaxSize(file: File) {
    if (file instanceof File && file.size > this.maxSize) {
      return true;
    }
    else return false;;
  }

  getToday(): string {
    return new Date().toISOString().split('T')[0]
  }

  specialCharacterValidation(event) {
    var regex = /[a-zA-Z0-9-_. ]/;
    var input = String.fromCharCode(event.keyCode);
    if (regex.test(input)) {
      return true;
    } else {
      event.preventDefault();
      return false;
    }
  }

  disableControl() {
    this.insertIdeaSubmission.controls['ownerempid'].disable();
    this.insertIdeaSubmission.controls['ideaownername'].disable();
    this.insertIdeaSubmission.controls['designationname'].disable();
    this.insertIdeaSubmission.controls['client'].disable();
    this.insertIdeaSubmission.controls['departmentname'].disable();
    this.insertIdeaSubmission.controls['hod'].disable();
    this.insertIdeaSubmission.controls['locationname'].disable();
    this.insertIdeaSubmission.controls['managerfeedback'].disable();
  }
}
