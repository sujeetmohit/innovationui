import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NewIdeaSubmissionComponent } from './new-idea-submission/new-idea-submission.component';
import { ApprovedIdeaSubmissionListComponent } from './ApprovedIdeaSubmissionList/approved-idea-submission-list/approved-idea-submission-list.component';
import { ViewIdeaSubmissionComponent } from './View-idea-submission/view-idea-submission/view-idea-submission.component';
import { SubmittedIdeaSubmissionComponent } from './submitted-idea-submission/submitted-idea-submission.component';
import { UpdateIdeaSubmissionComponent } from './updateIdeaSubmission/update-idea-submission/update-idea-submission.component';

const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Base'
    },
    children: [
      {
        path: 'newIdeaSubmission',
        component: NewIdeaSubmissionComponent,
        data: {
          title: 'view'
        }
      },
      {
        path: 'allIdeaSubmission',
        component: ApprovedIdeaSubmissionListComponent
      },
      {
        path: 'viewIdeaSubmission',
        component: ViewIdeaSubmissionComponent
      },
      {
        path: 'submittedIdeaSubmission',
        component: SubmittedIdeaSubmissionComponent
      },
      {
        path: 'updateIdeaSubmission',
        component: UpdateIdeaSubmissionComponent
      }
    ]
  }
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class IdeaSubmissionRoutingModule { }
