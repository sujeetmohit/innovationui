import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { Router, ActivatedRoute } from '@angular/router';
import { RolesManagementService } from '../../../../Services/Roles Service/roles-management.service';
import { FormBuilder } from '@angular/forms';
import { AppLoaderService } from '../../../../containers/app-loader/app-loader.service';
import { GlobalVariables } from '../../../../Services/utility/globalVariables';
import { IdeaSubmission } from '../../../../Services/IdeaSubmission/ideasubmissionservice';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-approved-idea-submission-list',
  templateUrl: './approved-idea-submission-list.component.html',
  styleUrls: ['./approved-idea-submission-list.component.css']
})
export class ApprovedIdeaSubmissionListComponent implements OnInit {

  hiddenField: boolean = false;
  ideaSubmissionList: IdeaSubmissionList;
  userName: string = '';


  displayedColumns: string[] = [
    "id",
    "createddate",
    "ownerempid",
    "designationname",
    "projecttypeName",
    "projectstatusbyManager",
    "projectstatusbyBtteam",
    "createdbyName",
    "action",
  ];

  dataSource: MatTableDataSource<IdeaSubmissionList>;

  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  public errorMessageStatus = '';
  public successMessageStatus = '';
  public userEmail;
  public isAdmin = false;
  public isHrManager = false;
  public isAssociate = false;

  constructor(
    private formBuilder: FormBuilder,
    private activatedRoute: ActivatedRoute,
    public datepipe: DatePipe,
    private router: Router,
    private rolesManagementService: RolesManagementService,
    private loader: AppLoaderService,
    private globalService: GlobalVariables,
    private ideaSubmission: IdeaSubmission) {
    this.loader.open();
    this.userName = this.globalService.getUserEmail();
    this.isAdmin = this.rolesManagementService.isAdmin;
    this.isHrManager = this.rolesManagementService.isHrManager;
    this.isAssociate = this.rolesManagementService.isAssociate;
  }

  ngOnInit() {
    this.getApprovedIdeaSubmission();
  }

  getApprovedIdeaSubmission() {
    this.loader.open();
    this.ideaSubmission.getApprovedIdeaSubmission(this.userName, 1).subscribe((data) => {
      this.dataSource = new MatTableDataSource(data["body"]);
      this.ideaSubmissionList = data["body"];
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
      if (this.dataSource.data.length === 0) {
        this.hiddenField = true;
      } else {
        this.hiddenField = false;
      }
      this.loader.close();
    }, (error) => {
      this.loader.close();
    });
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  viewIdeaSubmission(rowData: any) {
    this.loader.open();
    this.router.navigate(["../viewIdeaSubmission"], {
      queryParams: { rowId: btoa(rowData.id) },
      relativeTo: this.activatedRoute,
    });
  }

}
export interface IdeaSubmissionList {
  createddate: string;
  ownerempid: string;
  designationname: string;
  projecttypeName: string;
  projectstatusbyManager: string;
  projectstatusbyBtteam: string;
  createdbyName: string;
}


