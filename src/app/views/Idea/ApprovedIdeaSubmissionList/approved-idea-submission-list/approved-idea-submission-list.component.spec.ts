import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApprovedIdeaSubmissionListComponent } from './approved-idea-submission-list.component';

describe('ApprovedIdeaSubmissionListComponent', () => {
  let component: ApprovedIdeaSubmissionListComponent;
  let fixture: ComponentFixture<ApprovedIdeaSubmissionListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApprovedIdeaSubmissionListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApprovedIdeaSubmissionListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
