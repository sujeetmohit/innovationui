import { Component, OnInit, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { RolesManagementService } from '../../../../Services/Roles Service/roles-management.service';
import { AppLoaderService } from '../../../../containers/app-loader/app-loader.service';
import { GlobalVariables } from '../../../../Services/utility/globalVariables';
import { IdeaSubmission } from '../../../../Services/IdeaSubmission/ideasubmissionservice';

@Component({
  selector: 'app-view-idea-submission',
  templateUrl: './view-idea-submission.component.html',
  styleUrls: ['./view-idea-submission.component.css']
})
export class ViewIdeaSubmissionComponent implements OnInit {
  @Input() max: any;

  today = new Date();
  allowedExtensions = ['doc', 'docx', 'pdf', 'msg', 'pst', 'edb', 'ost', 'eml', 'mbox'];
  maxSize = 5000000;
  ideaSubmissionId: string;
  ideaSubmissionStatusUpdate: FormGroup;
  public errorMessageStatus = '';
  public successMessageStatus = '';
  public userEmail;
  public isAdmin = false;
  public isHrManager = false;
  showDownloadIdeaSubmissionButton = false;
  submitted = false;

  constructor(
    private formBuilder: FormBuilder,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private rolesManagementService: RolesManagementService,
    private loader: AppLoaderService,
    private globalService: GlobalVariables,
    private ideaSubmission: IdeaSubmission) {
    this.loader.open();

    this.today.setDate(this.today.getDate());
    this.ideaSubmissionId = atob(this.activatedRoute.snapshot.queryParams.rowId);
    this.userEmail = this.globalService.getUserEmail();
    this.isAdmin = this.rolesManagementService.isAdmin;
    this.isHrManager = this.rolesManagementService.isHrManager;
  }

  ngOnInit(): void {
    this.successMessageStatus = this.globalService.getPhraseToShow();
    this.ideaSubmissionStatusUpdate = this.formBuilder.group({
      ownerempid: [""],
      ideaownername: [""],
      designationname: [""],
      client: [""],
      departmentname: [""],
      hod: [""],
      locationname: [""],
      ideaproposalname: [""],
      problemstatement: [""],
      levertechnologyName: [""],
      riskdescription: [""],
      benefits: [""],
      projectcommencementdate: [""],
      projectcompletiondate: [""],
      projecttypeName: [""],
      managerfeedback: ["", Validators.required],
    });

    this.disableControl();
    this.getIdeaSubmissionbyId();
  }

  getIdeaSubmissionbyId() {
    this.ideaSubmission.getIdeaSubmissionbyId(this.ideaSubmissionId).subscribe((response) => {
      if (response["body"] != null) {
        this.ideaSubmissionStatusUpdate.patchValue(response["body"][0]);
        if (response["body"][0].receiptfilename != "") {
          this.showDownloadIdeaSubmissionButton = true;
        }
      } else {
        this.router.navigate(["../approvedIdeaSubmissionList"], {
          relativeTo: this.activatedRoute,
        });
      }
      this.loader.close();
    }, (error) => {
      this.loader.close();
    });

  }

  onSubmit() {
    
    if (this.ideaSubmissionStatusUpdate.invalid) {
      return;
    }
  }

  onApproveClick() {
    if (this.ideaSubmissionStatusUpdate.invalid) {
      alert('Please fill BT Comments.');
      return;
    }
    this.loader.open();
    this.errorMessageStatus = '';
    //  Status 1 shows approval
    this.ideaSubmission.updateManagerStatus(this.ideaSubmissionId, 1, this.ideaSubmissionStatusUpdate.controls['managerfeedback'].value).subscribe((reponse) => {
      if (reponse["status"] === true) {
        this.globalService.setPhraseToShow("Idea Submission Status Updated Successfully.");
      }
      this.loader.close();
      this.router.navigate(["../submittedIdeaSubmission"], { relativeTo: this.activatedRoute });
    }, (error) => {
      this.errorMessageStatus = 'Failed to update the Idea Submission Status';
      this.loader.close();
    });
  }

  onRejectClick() {
    if (this.ideaSubmissionStatusUpdate.invalid) {
      alert('Please fill BT Comments.');
      return;
    }
    this.loader.open();
    this.errorMessageStatus = '';
    //  Status 2 shows reject
    this.ideaSubmission.updateManagerStatus(this.ideaSubmissionId, 2, this.ideaSubmissionStatusUpdate.controls['managerfeedback'].value).subscribe((reponse) => {
      if (reponse["status"] === true) {
        this.globalService.setPhraseToShow("Idea Submission Status Updated Successfully.");
      }
      this.loader.close();
      this.router.navigate(["../submittedIdeaSubmission"], { relativeTo: this.activatedRoute });
    }, (error) => {
      this.errorMessageStatus = 'Failed to update the Idea Submission Status';
      this.loader.close();
    });
  }

  onReturnClick() {
    if (this.ideaSubmissionStatusUpdate.invalid) {
      alert('Please fill BT Comments.');
      return;
    }
    this.loader.open();
    this.errorMessageStatus = '';
    //  Status 5 shows Return
    this.ideaSubmission.updateManagerStatus(this.ideaSubmissionId, 5, this.ideaSubmissionStatusUpdate.controls['managerfeedback'].value).subscribe((reponse) => {
      if (reponse["status"] === true) {
        this.globalService.setPhraseToShow("Idea Submission Status Updated Successfully.");
      }
      this.loader.close();
      this.router.navigate(["../submittedIdeaSubmission"], { relativeTo: this.activatedRoute });
    }, (error) => {
      this.errorMessageStatus = 'Failed to update the Idea Submission Status';
      this.loader.close();
    });
  }

  downloadIdeaSubmission() {
    window.location.href = this.ideaSubmission.baseUrl
      + "fileupload/ideaSubmission/downloadIdeaSubmission?ideaSubmissionId=" + this.ideaSubmissionId;
  }

  specialCharacterValidation(event) {
    var regex = /[a-zA-Z0-9-_. ]/;
    var input = String.fromCharCode(event.keyCode);
    if (regex.test(input)) {
      return true;
    } else {
      event.preventDefault();
      return false;
    }
  }

  disableControl() {
    this.ideaSubmissionStatusUpdate.controls['ownerempid'].disable();
    this.ideaSubmissionStatusUpdate.controls['ideaownername'].disable();
    this.ideaSubmissionStatusUpdate.controls['designationname'].disable();
    this.ideaSubmissionStatusUpdate.controls['client'].disable();
    this.ideaSubmissionStatusUpdate.controls['departmentname'].disable();
    this.ideaSubmissionStatusUpdate.controls['hod'].disable();
    this.ideaSubmissionStatusUpdate.controls['locationname'].disable();
    this.ideaSubmissionStatusUpdate.controls['ideaproposalname'].disable();
    this.ideaSubmissionStatusUpdate.controls['problemstatement'].disable();
    this.ideaSubmissionStatusUpdate.controls['levertechnologyName'].disable();
    this.ideaSubmissionStatusUpdate.controls['riskdescription'].disable();
    this.ideaSubmissionStatusUpdate.controls['benefits'].disable();
    this.ideaSubmissionStatusUpdate.controls['projectcommencementdate'].disable();
    this.ideaSubmissionStatusUpdate.controls['projectcompletiondate'].disable();
    this.ideaSubmissionStatusUpdate.controls['projecttypeName'].disable();

  }

}
