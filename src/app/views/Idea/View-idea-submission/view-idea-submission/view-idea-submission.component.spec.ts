import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewIdeaSubmissionComponent } from './view-idea-submission.component';

describe('ViewIdeaSubmissionComponent', () => {
  let component: ViewIdeaSubmissionComponent;
  let fixture: ComponentFixture<ViewIdeaSubmissionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewIdeaSubmissionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewIdeaSubmissionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
