import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { DatePipe, CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { MatNativeDateModule, MAT_DATE_LOCALE } from '@angular/material/core';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatBadgeModule } from '@angular/material/badge';
import { MatButtonModule } from '@angular/material/button';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatCardModule } from '@angular/material/card';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatChipsModule } from '@angular/material/chips';
import { MatStepperModule } from '@angular/material/stepper';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatDialogModule } from '@angular/material/dialog';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatListModule } from '@angular/material/list';
import { MatMenuModule } from '@angular/material/menu';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatRadioModule } from '@angular/material/radio';
import { MatRippleModule } from '@angular/material/core';
import { MatSelectModule } from '@angular/material/select';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatSliderModule } from '@angular/material/slider';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatSortModule } from '@angular/material/sort';
import { MatTableModule } from '@angular/material/table';
import { MatTabsModule } from '@angular/material/tabs';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatTreeModule } from '@angular/material/tree';
import { RichTextEditorAllModule } from '@syncfusion/ej2-angular-richtexteditor';
import { IdeaSubmissionRoutingModule } from './idea-routing';
import { SharedModule } from '../../containers/shared.module';
import { ViewIdeaSubmissionComponent } from './View-idea-submission/view-idea-submission/view-idea-submission.component';
import { NewIdeaSubmissionComponent } from './new-idea-submission/new-idea-submission.component';
import { ApprovedIdeaSubmissionListComponent } from './ApprovedIdeaSubmissionList/approved-idea-submission-list/approved-idea-submission-list.component';
import { RouterModule } from '@angular/router';
import { routes } from '../../app.routing';
import { IdeaSubmission } from '../../Services/IdeaSubmission/ideasubmissionservice';
import { SubmittedIdeaSubmissionComponent } from './submitted-idea-submission/submitted-idea-submission.component';
import { UpdateIdeaSubmissionComponent } from './updateIdeaSubmission/update-idea-submission/update-idea-submission.component';
import { IdeaSubmissionEntity } from '../../Modals/Idea/IdeaSubmission';


@NgModule({
  imports: [IdeaSubmissionRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    MatAutocompleteModule,
    MatBadgeModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatCardModule,
    MatCheckboxModule,
    MatChipsModule,
    MatStepperModule,
    MatDatepickerModule,
    MatDialogModule,
    MatExpansionModule,
    MatFormFieldModule,
    MatGridListModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatPaginatorModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatRippleModule,
    MatSelectModule,
    MatSidenavModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatSnackBarModule,
    MatSortModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule,
    MatTooltipModule,
    MatTreeModule,
    MatNativeDateModule,
    RichTextEditorAllModule,
    SharedModule
  ],
  declarations: [NewIdeaSubmissionComponent,
    ApprovedIdeaSubmissionListComponent,
    ViewIdeaSubmissionComponent,
    SubmittedIdeaSubmissionComponent,
    UpdateIdeaSubmissionComponent
  ],
  providers: [DatePipe, IdeaSubmission,
    IdeaSubmissionEntity],
  entryComponents: [],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class IdeaSubmissionModule { }
