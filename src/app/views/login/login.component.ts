import { AppLoaderService } from './../../containers/app-loader/app-loader.service';
import { Component, OnInit } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { LoginService } from "../../Services/Login Service/login.service";
import { Subscription } from 'rxjs';
import { RolesManagementService } from '../../Services/Roles Service/roles-management.service';
import Utils from '../../Services/utility/utils';
import { GlobalVariables } from '../../Services/utility/globalVariables';

@Component({
  selector: "app-dashboard",
  templateUrl: "login.component.html",
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  submitted = false;
  token: string;
  failedLogin: boolean = false;
  isHidden: boolean = false;
  invalidCredentials = "Invalid username or password..!";
  subscription: Subscription;
  public successMessageStatus = '';
  public errorMessageStatus = '';

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private loginService: LoginService,
    private rolesManagementService: RolesManagementService,
    private loader: AppLoaderService,
    private globalService: GlobalVariables) { }

  ngOnInit() {
    this.initRoles();
    // this.loader.open();
    this.loginForm = this.formBuilder.group({
      username: ["", Validators.required],
      password: ["", Validators.required],
    });
  }

  onSubmit() {
    this.submitted = true;
    this.errorMessageStatus = '';
    if (this.loginForm.invalid) {
      return;
    }
    this.loader.open();


    this.loginService.loginRequest(this.loginForm.value).subscribe((data) => {
      if (data != null && data["status"] == true && data["token"] != "") {
        this.isHidden = this.loginService.successLogin;
        this.globalService.setUserEmail(data["email"]);
        this.globalService.setEmpId(data["ais_id"]);

        this.globalService.setRoles(JSON.stringify(data["authorities"]));
        this.globalService.setToken(data["token"]);

        //store user roles in login service
        this.loginService.rolesList.push(data["authorities"]);
        // show role based actions
        this.rolesManagementService.userRoles.push(data["authorities"]);
        this.rolesManagementService.roleBasedActions();

        // alert("routing to dashboard screen");
        this.router.navigate(["dashboard"]);

      } else {
        this.isHidden = this.loginService.successLogin;
        return false;
      }
    }, (error) => {
      Utils.log("Invalid username or password..!");
      this.errorMessageStatus = "Invalid username or password..!";
      this.loader.close();

      // this.globalService.setUserEmail('sujeet.singh@aisinfo.com');
      // this.globalService.setEmpId('AISG003');
      // this.globalService.setRoles(JSON.stringify([{authority:'admin'}]));
      // this.loginService.rolesList.push([{authority:'admin'}]);
      // this.rolesManagementService.userRoles.push([{authority:'admin'}]);
      // this.rolesManagementService.roleBasedActions();
      // this.router.navigate(["dashboard"]);

      // this.globalService.setUserEmail('seema.vats@aisinfo.com');
      // this.globalService.setEmpId('AISG002');
      // this.globalService.setRoles(JSON.stringify([{authority:'manager'}]));
      // this.loginService.rolesList.push([{authority:'manager'}]);
      // this.rolesManagementService.userRoles.push([{authority:'manager'}]);
      // this.rolesManagementService.roleBasedActions();
      // this.router.navigate(["dashboard"]);

    });

  }

  initRoles() {
    this.rolesManagementService.isAdmin = false;
    this.rolesManagementService.isAssociate = false;
    this.rolesManagementService.isFinance = false;
    this.rolesManagementService.isHr = false;
    this.rolesManagementService.isHrManager = false;
    this.rolesManagementService.isInterviewer = false;
    this.rolesManagementService.isManager = false;
    this.rolesManagementService.isRecruiter = false;
    this.rolesManagementService.isBTAdmin = false;
  }
}
