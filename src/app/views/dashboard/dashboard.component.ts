import { RolesManagementService } from './../../Services/Roles Service/roles-management.service';
import { GlobalVariables } from './../../Services/utility/globalVariables';
import { Dashboard, ProjectCharter, IdeaSubmission } from './../../Modals/contsants';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DashboardService } from '../../Services/dashboard/dashboard.service';
import * as _ from 'lodash';
import Utils from '../../Services/utility/utils'
import { utils } from 'protractor';
import { AppLoaderService } from '../../containers/app-loader/app-loader.service';
import { ChartType } from 'chart.js';
import { MultiDataSet, Label } from 'ng2-charts';

@Component({
  templateUrl: 'dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {


  radioModel: string = 'Month';
  userName: string;
  sessionToken: string;
  roletypeId: number;

  viewIdea: any[] = [550, 300];
  viewProject: any[] = [550, 300];
  legend: boolean = true;
  legendPosition: string = 'below';
  colorSchemeIdea = {
    domain: ['#c8ced3', '#63c2de', '#f8cb00', '#f86c6b','#1E90FF']
  };

  colorSchemeProject = {
    domain: ['#f86c6b', '#20c997', '#20a8d8', '#aae3f5', '#eb34b4']
  };

  public departments = [];

  // Employee Details

  showXAxis: boolean = true;
  showYAxis: boolean = true;
  gradient: boolean = false;
  showLegend: boolean = true;
  showXAxisLabel: boolean = true;
  xAxisLabel: string = 'Department';
  showYAxisLabel: boolean = true;
  yAxisLabel: string = 'Employee Count';
  animations: boolean = true;

  public dashboardData;
  public ideaSubmissionResultSet = [];
  public projectcharterResultSet = [];
  public showAssociateGraph = false;

  constructor(private loader: AppLoaderService,
    private dashboardService: DashboardService,
    private globalService: GlobalVariables,
    public roleManagementService: RolesManagementService) {
    this.userName = this.globalService.getUserName();
  }

  ngOnInit(): void {
    this.getDashboardDetails();
  }

  getDashboardDetails() {
    this.loader.open();
    this.dashboardService.getDashboardDetails().subscribe((success) => {
      this.dashboardData = success.body;
      this.ideaSubmissionDetails(success.body.reportIdea);
      this.projectCharterDetails(success.body.reportProject);
      this.loader.close();
    }, (error) => {
    });
  }


  ideaSubmissionDetails(tempIdea) {

    // let resultSet = [];
    // let obj = {};
    // for (let index = 0; index < data.length; index++) {
    //   obj = {};
    //   obj['name'] = data[index].projectstatusName;
    //   obj['series'] = [];
    //   obj['series'].push({ 'name': IdeaSubmission.approved, 'value': Number(data[index]['approved']), 'projectstatusId': data[index].projectstatusId });
    //   obj['series'].push({ 'name': IdeaSubmission.rejected, 'value': Number(data[index]['rejected']), 'projectstatusId': data[index].projectstatusId });
    //   obj['series'].push({ 'name': IdeaSubmission.returned, 'value': Number(data[index]['returned']), 'projectstatusId': data[index].projectstatusId });
    //   obj['series'].push({ 'name': IdeaSubmission.total, 'value': Number(data[index]['total']), 'projectstatusId': data[index].projectstatusId});

    //   resultSet.push(obj);
    // }
    // this.ideaSubmissionResultSet = resultSet;

    let data = _.cloneDeep(tempIdea);
    let unique = [];
    for (const key in IdeaSubmission) {
      unique.push({ name: IdeaSubmission[key], value: _.sumBy(data, key) });
    }
  
    this.ideaSubmissionResultSet = unique;
  }

  projectCharterDetails(tempCharter) {
 
    let data = _.cloneDeep(tempCharter);
    let unique = [];
    for (const key in ProjectCharter) {
      unique.push({ name: ProjectCharter[key], value: _.sumBy(data, key) });
    }
   
    this.projectcharterResultSet = unique;
  }

  onDeactivate(event) {
  }

  onActivate(event) {
  }

  onSelect(event) {
  }

}
