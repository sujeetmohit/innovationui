import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NewProjectCharterComponent } from './NewProjectCharter/new-project-charter/new-project-charter.component';
import { ViewProjectCharterComponent } from './ViewProjectCharter/view-project-charter/view-project-charter.component';
import { UpdateProjectCharterComponent } from './UpdateProjectCharter/update-project-charter/update-project-charter.component';
import { SubmittedProjectCharterComponent } from './SubmittedProjectCharter/submitted-project-charter/submitted-project-charter.component';
import { CreateProjectCharterComponent } from './CreateProjectCharterbyIdea/create-project-charter/create-project-charter.component';
import { ProjectCharterApprovalComponent } from './ProjectCharterApproval/project-charter-approval/project-charter-approval.component';

const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Base'
    },
    children: [
      {
        path: 'newProjectCharter',
        component: NewProjectCharterComponent,
        data: {
          title: 'view'
        }
      },
      {
        path: 'createProjectCharter',
        component: CreateProjectCharterComponent
      },
      {
        path: 'updateProjectCharter',
        component: UpdateProjectCharterComponent
      },
      {
        path: 'viewProjectCharter',
        component: ViewProjectCharterComponent
      },
      {
        path: 'submittedProjectCharter',
        component: SubmittedProjectCharterComponent
      },
      {
        path: 'projectCharterApproval',
        component: ProjectCharterApprovalComponent
      }
    ]
  }
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProjectCharterRoutingModule { }
