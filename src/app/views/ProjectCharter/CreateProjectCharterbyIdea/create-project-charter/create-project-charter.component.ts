import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { MAT_DATE_LOCALE, DateAdapter, MAT_DATE_FORMATS } from '@angular/material/core';
// import { MomentDateAdapter } from '@angular/material-moment-adapter';
import { LinkService, HtmlEditorService, TableService, ToolbarService, ImageService } from '@syncfusion/ej2-angular-richtexteditor';
import { Validators, FormGroup, FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { RolesManagementService } from '../../../../Services/Roles Service/roles-management.service';
import { AppLoaderService } from '../../../../containers/app-loader/app-loader.service';
import { GlobalVariables } from '../../../../Services/utility/globalVariables';
import { ProjectCharterService } from '../../../../Services/ProjectCharter/projectCharterService';
import { ProjectCharterEntity, ProjectCharterRevisionHistory, ProjectCharterDocuments, ProjectCharterKeyStakeHolders, ProjectCharterSteeringComtProjectTeam, ProjectCharterDmaicphaseToll, ProjectCharterBenefits, ProjectCharterResultArea, ProjectCharterSteeringCommitee } from '../../../../Modals/ProjectCharter/projectCharter';
import { IdeaSubmission } from '../../../../Services/IdeaSubmission/ideasubmissionservice';
import Utils from '../../../../Services/utility/utils';

export const MY_DATE_FORMATS = {
  parse: {
    dateInput: 'DD/MM/YYYY',
  },
  display: {
    dateInput: 'DD/MM/YYYY',
    monthYearLabel: 'MMMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY'
  },
};

@Component({
  selector: 'app-create-project-charter',
  templateUrl: './create-project-charter.component.html',
  styleUrls: ['./create-project-charter.component.css'],

  providers: [{ provide: MAT_DATE_LOCALE, useValue: "en-US" },
  // {
  //   provide: DateAdapter,
  //   useClass: MomentDateAdapter,
  //   deps: [MAT_DATE_LOCALE],
  // },
  { provide: MAT_DATE_FORMATS, useValue: MY_DATE_FORMATS },
    ToolbarService, LinkService, ImageService, HtmlEditorService, TableService],
})
export class CreateProjectCharterComponent implements OnInit {

  @Input() max: any;

  hiddenField = false;
  today = new Date();
  projectTypeId: string;
  leverId: string;
  ideasubmissionId: string;
  projectIdGenerated: number;
  createProjectCharter: FormGroup;
  public errorMessageStatus = '';
  public successMessageStatus = '';
  submitted = false;
  DMAICTollGateList = new Array();
  leverTechnologyList = new Array();
  resulAreaNameList = new Array();
  hiddenProjectCharterAccordion = false;

  blankResultAreaArray: Array<ProjectCharterResultArea> = [];
  blankBenefitsArray: Array<ProjectCharterBenefits> = [];
  dmailTollPhaseArray: Array<ProjectCharterDmaicphaseToll> = [];
  steeringcomiteeTeamArray: Array<ProjectCharterSteeringComtProjectTeam> = [];
  keyStakeHoldersArray: Array<ProjectCharterKeyStakeHolders> = [];
  documentsUpdatedArray: Array<ProjectCharterDocuments> = [];
  revisionHistoryArray: Array<ProjectCharterRevisionHistory> = [];

  public userEmail;
  public isAdmin = false;
  public isHrManager = false;


  @Output() successMessage = new EventEmitter<string>();
  @Output() errorMessage = new EventEmitter<string>();

  constructor(
    private formBuilder: FormBuilder,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private rolesManagementService: RolesManagementService,
    private loader: AppLoaderService,
    private globalService: GlobalVariables,
    private projectCharterService: ProjectCharterService,
    private ideaSubmission: IdeaSubmission,
    private projectCharterObj: ProjectCharterEntity,
    private projectCharterSteeringCommiteeObj: ProjectCharterSteeringCommitee,
  ) {   
    this.loader.open();
    this.projectTypeId = atob(this.activatedRoute.snapshot.queryParams.projectTypeId);

    this.leverId = atob(this.activatedRoute.snapshot.queryParams.leverId);
    this.ideasubmissionId = atob(this.activatedRoute.snapshot.queryParams.ideasubmissionId);

    this.today.setDate(this.today.getDate());
    this.userEmail = this.globalService.getUserEmail();

    if (Number(this.projectTypeId) == 1 || Number(this.projectTypeId) == 6) {
      this.hiddenProjectCharterAccordion = true;
    }

    this.isAdmin = this.rolesManagementService.isAdmin;
    this.isHrManager = this.rolesManagementService.isHrManager;

    //Lever Technology dropdown Onload
    this.ideaSubmission.getLeverTechnologyDetails().subscribe((data) => {
      this.leverTechnologyList = data["body"];
      this.onLeverTechnologySelect();
    });

    //Lever Technology dropdown Onload
    this.projectCharterService.getDMAICPhaseToll().subscribe((data) => {
      this.DMAICTollGateList = data["body"];
      this.onDMAILTollSelect();
    });

    //Project Type dropdown Onload
    this.projectCharterService.getResultAreaType().subscribe((data) => {
      this.resulAreaNameList = data["body"];
      this.onResultAreaTypeSelect();
    });
  }

  ngOnInit() {
    this.successMessageStatus = this.globalService.getPhraseToShow();
    this.createProjectCharter = this.formBuilder.group({
      projectName: [""],
      projectSponsor: [""],
      projectManager: [""],
      projectapprovalDate: [""],
      lastrevisionDate: [""],
      problemStatement: [""],
      projectDescription: [""],
      inScope: [""],
      outScope: [""],
      businessCase: [""],
      leverId: [""],
      projectDeliverable: [""],
      projectRisk: [""],
      champion: [""],
      masterblackBelt: [""],
      blackBelt: [""],
      ideasubmissionId:[""],

    });
    // this.createProjectCharter.controls['leverId'].disable();
    this.createProjectCharter.controls['leverId'].setValue(this.leverId)
    this.loader.close();
  }


  onSubmit() {
    this.submitted = true;
    this.loader.open();

    this.projectCharterObj.id = 0;
    this.projectCharterObj.projecttypeId = this.projectTypeId;
    this.projectCharterObj.projectName = this.createProjectCharter.value['projectName'];
    this.projectCharterObj.projectSponsor = this.createProjectCharter.value['projectSponsor'];
    this.projectCharterObj.projectManager = this.createProjectCharter.value['projectManager'];
    this.projectCharterObj.projectapprovalDate = this.createProjectCharter.value['projectapprovalDate'];
    this.projectCharterObj.lastrevisionDate = this.createProjectCharter.value['lastrevisionDate'];
    this.projectCharterObj.problemStatement = this.createProjectCharter.value['problemStatement'];
    this.projectCharterObj.inScope = this.createProjectCharter.value['inScope'];
    this.projectCharterObj.outScope = this.createProjectCharter.value['outScope'];
    this.projectCharterObj.projectDescription = this.createProjectCharter.value['projectDescription'];
    this.projectCharterObj.buisnessCase = this.createProjectCharter.value['businessCase'];
    this.projectCharterObj.projectRisk = this.createProjectCharter.value['projectRisk'];
    this.projectCharterObj.projectDeliverable = this.createProjectCharter.value['projectDeliverable']
    this.projectCharterObj.leverId = this.createProjectCharter.value['leverId'];
    this.projectCharterObj.ideasubmissionId = this.ideasubmissionId;
    this.projectCharterObj.createdBy = this.userEmail;

    this.projectCharterService.InsertProjectCharterDetails(this.projectCharterObj).subscribe((data) => {

      let responseBody: string = data["body"];
      let array: string[] = responseBody.split('= ');
      this.projectIdGenerated = Number(array[1]);

      this.projectCharterSteeringCommiteeObj.projectId = this.projectIdGenerated;
      this.projectCharterSteeringCommiteeObj.projecttypeId = Number(this.projectTypeId);
      this.projectCharterSteeringCommiteeObj.champion = this.createProjectCharter.value['champion'];
      this.projectCharterSteeringCommiteeObj.blackBelt = this.createProjectCharter.value['blackBelt'];
      this.projectCharterSteeringCommiteeObj.masterblackBelt = this.createProjectCharter.value['masterblackBelt'];
      this.projectCharterSteeringCommiteeObj.createdBy = this.userEmail;

      this.projectCharterSteeringCommiteeObj.projectId = this.projectIdGenerated;

      this.projectCharterService.InsertProjectCharterResultArea(this.blankResultAreaArray, this.projectIdGenerated, Number(this.projectTypeId)).subscribe((data) => {
      });

      this.projectCharterService.InsertProjectCharterDocumentDetail(this.documentsUpdatedArray, this.projectIdGenerated, Number(this.projectTypeId)).subscribe((data) => {
      });

      this.projectCharterService.InsertProjectCharterSteeringCommittedetail(this.projectCharterSteeringCommiteeObj, this.projectIdGenerated, Number(this.projectTypeId)).subscribe((data) => {
      });
      this.projectCharterService.InsertProjectCharterDmaicphaseToll(this.dmailTollPhaseArray, this.projectIdGenerated, Number(this.projectTypeId)).subscribe((data) => {
      });
      this.projectCharterService.InsertProjectCharterSteeringCommitteTeamdetail(this.steeringcomiteeTeamArray, this.projectIdGenerated, Number(this.projectTypeId)).subscribe((data) => {
      });
      this.projectCharterService.InsertProjectCharterBenefitDetail(this.blankBenefitsArray, this.projectIdGenerated, Number(this.projectTypeId)).subscribe((data) => {
      });
      this.projectCharterService.InsertProjectCharterKeyStakeHolderdetail(this.keyStakeHoldersArray, this.projectIdGenerated, Number(this.projectTypeId)).subscribe((data) => {
      });

      this.projectCharterService.InsertProjectCharterRevisionHistorydetail(this.revisionHistoryArray, this.projectIdGenerated, Number(this.projectTypeId)).subscribe((data) => {


        if (data["status"] === true) {
          this.successMessage.emit("Project Charter details updated successfully.");
          this.router.navigate(["../submittedProjectCharter"], {
            relativeTo: this.activatedRoute,
          });
        }
        this.loader.close();
      }, (error) => {
        this.errorMessage.emit("Failed to update project charter details");
        // this.loader.close();
      });
    });
  }

  getProjectCharterDetails() {
    this.projectCharterService
      .getProjectCharterByProjectIdDetails(1)
      .subscribe((response) => {
        if (Utils.isValidInput(response["body"])) {
          this.createProjectCharter.patchValue(response["body"][0]);
          this.loader.close();
        }
        else {
          this.errorMessage.emit("No Record Found..!!");
          this.loader.close();
        }
      });
  }

  onDMAILTollSelect() {
    let leverList = this.DMAICTollGateList;
    // let technologyName = _.find(leverList, { 'id': Number(this.insertIdeaSubmission.controls['leverid'].value) }).technologyName;
    // this.insertIdeaSubmission.controls['levertechnologyName'].setValue(technologyName);
  }

  onResultAreaTypeSelect() {
    let projectTypeList = this.resulAreaNameList;
    // let projectTypeName = _.find(projectTypeList, { 'id': Number(this.insertIdeaSubmission.controls['projecttypeid'].value) }).projecttypeName;
    // this.insertIdeaSubmission.controls['projecttypeName'].setValue(projectTypeName);
  }

  onLeverTechnologySelect() {
    let leverList = this.leverTechnologyList;
    // let technologyName = _.find(leverList, { 'id': Number(this.insertIdeaSubmission.controls['leverid'].value) }).technologyName;
    // this.insertIdeaSubmission.controls['levertechnologyName'].setValue(technologyName);
  }

  public addBlankRow(type) {
    if (type === 'resultArea') {
      this.addResultAreaDetail();
    } else if (type === 'benefits') {
      this.addBenefitsDetail();
    } else if (type === 'dmailTollPhase') {
      this.addDmaicTollPhaseDetail();
    } else if (type === 'steeringcomiteeTeam') {
      this.addSteeringComitteDetail();
    }
    else if (type === 'keyStakeHolders') {
      this.addKeyStakeHoldersDetail();
    }
    else if (type === 'documentsUpdated') {
      this.addDocumentUpdateDetail();
    }
    else if (type === 'revisionHistory') {
      this.addRevisionHistoryDetail();
    }
  }

  public deleteRow(index, type) {
    if (type === 'resultArea') {
      this.blankResultAreaArray.splice(index, 1);
    } else if (type === 'benefits') {
      this.blankBenefitsArray.splice(index, 1);
    } else if (type === 'dmailTollPhase') {
      this.dmailTollPhaseArray.splice(index, 1);
    } else if (type === 'steeringcomiteeTeam') {
      this.steeringcomiteeTeamArray.splice(index, 1);
    }
    else if (type === 'keyStakeHolders') {
      this.keyStakeHoldersArray.splice(index, 1);
    }
    else if (type === 'documentsUpdated') {
      this.documentsUpdatedArray.splice(index, 1);
    }
    else if (type === 'revisionHistory') {
      this.revisionHistoryArray.splice(index, 1);
    }
  }

  private addResultAreaDetail() {
    const resulAreablankRowData = new ProjectCharterResultArea();
    (resulAreablankRowData.projectId = this.projectIdGenerated),
      (resulAreablankRowData.projecttypeId = Number(this.projectTypeId)),
      (resulAreablankRowData.resultareaId = 0),
      (resulAreablankRowData.resultareaName = ""),
      (resulAreablankRowData.description = ""),
      (resulAreablankRowData.createdBy = this.userEmail),
      this.blankResultAreaArray.push(resulAreablankRowData);
  }

  private addBenefitsDetail() {
    const benefitsblankRowData = new ProjectCharterBenefits();
    (benefitsblankRowData.projectId = this.projectIdGenerated),
      (benefitsblankRowData.projecttypeId = Number(this.projectTypeId)),
      (benefitsblankRowData.kpi = ""),
      (benefitsblankRowData.baseline = ""),
      (benefitsblankRowData.goal = ""),
      (benefitsblankRowData.createdBy = this.userEmail),
      this.blankBenefitsArray.push(benefitsblankRowData);
  }

  private addDmaicTollPhaseDetail() {
    const dmailPhasetollblankRowData = new ProjectCharterDmaicphaseToll();
    (dmailPhasetollblankRowData.projectId = this.projectIdGenerated),
      (dmailPhasetollblankRowData.projecttypeId = Number(this.projectTypeId)),
      (dmailPhasetollblankRowData.dmaicId = 0),
      (dmailPhasetollblankRowData.startDate = ""),
      (dmailPhasetollblankRowData.endDate = ""),
      (dmailPhasetollblankRowData.tollgatereviewDate = ""),
      (dmailPhasetollblankRowData.createdBy = this.userEmail),
      this.dmailTollPhaseArray.push(dmailPhasetollblankRowData);
  }

  private addSteeringComitteDetail() {
    const steeringComTeamDetailArray = new ProjectCharterSteeringComtProjectTeam();
    (steeringComTeamDetailArray.projectId = this.projectIdGenerated),
      (steeringComTeamDetailArray.projecttypeId = Number(this.projectTypeId)),
      (steeringComTeamDetailArray.employeeId = ""),
      (steeringComTeamDetailArray.employeeName = ""),
      (steeringComTeamDetailArray.createdBy = this.userEmail),
      this.steeringcomiteeTeamArray.push(steeringComTeamDetailArray);
  }

  private addKeyStakeHoldersDetail() {
    const keyStakeHoldersblankRowData = new ProjectCharterKeyStakeHolders();
    (keyStakeHoldersblankRowData.projectId = this.projectIdGenerated),
      (keyStakeHoldersblankRowData.projecttypeId = Number(this.projectTypeId)),
      (keyStakeHoldersblankRowData.ksName = ""),

      (keyStakeHoldersblankRowData.successCriteria = ""),
      (keyStakeHoldersblankRowData.createdBy = this.userEmail),
      this.keyStakeHoldersArray.push(keyStakeHoldersblankRowData);
  }

  private addDocumentUpdateDetail() {
    const documentsblankRowData = new ProjectCharterDocuments();
    (documentsblankRowData.projectId = this.projectIdGenerated),
      (documentsblankRowData.projecttypeId = Number(this.projectTypeId)),
      (documentsblankRowData.documentName = ""),
      (documentsblankRowData.documentNo = ""),
      (documentsblankRowData.resp = ""),
      (documentsblankRowData.requestedOn = ""),
      (documentsblankRowData.updatedOn = ""),
      (documentsblankRowData.respRepository = ""),
      (documentsblankRowData.createdBy = this.userEmail),
      this.documentsUpdatedArray.push(documentsblankRowData);
  }

  private addRevisionHistoryDetail() {
    const revisionHisblankRowData = new ProjectCharterRevisionHistory();
    (revisionHisblankRowData.projectId = this.projectIdGenerated),
      (revisionHisblankRowData.projecttypeId = Number(this.projectTypeId)),
      (revisionHisblankRowData.description = ""),
      (revisionHisblankRowData.dateModified = ""),
      (revisionHisblankRowData.checkedBy = ""),
      (revisionHisblankRowData.approvedBy = ""),
      (revisionHisblankRowData.createdBy = this.userEmail),
      this.revisionHistoryArray.push(revisionHisblankRowData);
  }

  checkInteger(event) {
    return (event.charCode == 8 || event.charCode == 0 || event.charCode == 13) ? null : event.charCode >= 48 && event.charCode <= 57
  }

  getToday(): string {
    return new Date().toISOString().split('T')[0]
  }

  validateEmail(event) {
    var reg = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
    if (reg.test(event.target.value) == false) {
      alert('Please Enter Valid Email Address');
      event.target.value = "";
      return false;
    }
    return true;
  }

}
