import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateProjectCharterComponent } from './create-project-charter.component';

describe('CreateProjectCharterComponent', () => {
  let component: CreateProjectCharterComponent;
  let fixture: ComponentFixture<CreateProjectCharterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateProjectCharterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateProjectCharterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
