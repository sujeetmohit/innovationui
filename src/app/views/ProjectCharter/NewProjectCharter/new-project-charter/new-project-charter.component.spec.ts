import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewProjectCharterComponent } from './new-project-charter.component';

describe('NewProjectCharterComponent', () => {
  let component: NewProjectCharterComponent;
  let fixture: ComponentFixture<NewProjectCharterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewProjectCharterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewProjectCharterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
