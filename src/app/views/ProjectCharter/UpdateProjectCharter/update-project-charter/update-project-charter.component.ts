import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { MAT_DATE_LOCALE, DateAdapter, MAT_DATE_FORMATS } from '@angular/material/core';
// import { MomentDateAdapter } from '@angular/material-moment-adapter';
import { HtmlEditorService, TableService, ImageService, LinkService, ToolbarService } from '@syncfusion/ej2-angular-richtexteditor';
import { FormGroup, FormBuilder } from '@angular/forms';
import { ProjectCharterResultArea, ProjectCharterBenefits, ProjectCharterDmaicphaseToll, ProjectCharterSteeringComtProjectTeam, ProjectCharterKeyStakeHolders, ProjectCharterDocuments, ProjectCharterRevisionHistory, ProjectCharterEntity, ProjectCharterSteeringCommitee } from '../../../../Modals/ProjectCharter/projectCharter';
import { ActivatedRoute, Router } from '@angular/router';
import { RolesManagementService } from '../../../../Services/Roles Service/roles-management.service';
import { AppLoaderService } from '../../../../containers/app-loader/app-loader.service';
import { GlobalVariables } from '../../../../Services/utility/globalVariables';
import { ProjectCharterService } from '../../../../Services/ProjectCharter/projectCharterService';
import { IdeaSubmission } from '../../../../Services/IdeaSubmission/ideasubmissionservice';
import Utils from '../../../../Services/utility/utils';


export const MY_DATE_FORMATS = {
  parse: {
    dateInput: 'DD/MM/YYYY',
  },
  display: {
    dateInput: 'DD/MM/YYYY',
    monthYearLabel: 'MMMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY'
  },
};

@Component({
  selector: 'app-update-project-charter',
  templateUrl: './update-project-charter.component.html',
  styleUrls: ['./update-project-charter.component.css'],

  providers: [{ provide: MAT_DATE_LOCALE, useValue: "en-US" },
  // {
  //   provide: DateAdapter,
  //   useClass: MomentDateAdapter,
  //   deps: [MAT_DATE_LOCALE],
  // },
  { provide: MAT_DATE_FORMATS, useValue: MY_DATE_FORMATS },
    ToolbarService, LinkService, ImageService, HtmlEditorService, TableService],
})
export class UpdateProjectCharterComponent implements OnInit {

  @Input() max: any;

  hiddenField = false;
  today = new Date();
  allowedExtensions = ['doc', 'docx', 'pdf', 'msg', 'pst', 'edb', 'ost', 'eml', 'mbox'];
  maxSize = 5000000;
  projectTypeId: string;
  projectIdGenerated: string;
  steeringId: number;
  updateProjectCharter: FormGroup;
  public errorMessageStatus = '';
  public successMessageStatus = '';
  submitted = false;
  DMAICTollGateList = new Array();
  leverTechnologyList = new Array();
  resulAreaNameList = new Array();

  blankResultAreaArray: Array<ProjectCharterResultArea> = [];
  blankBenefitsArray: Array<ProjectCharterBenefits> = [];
  dmailTollPhaseArray: Array<ProjectCharterDmaicphaseToll> = [];
  steeringcomiteeTeamArray: Array<ProjectCharterSteeringComtProjectTeam> = [];
  keyStakeHoldersArray: Array<ProjectCharterKeyStakeHolders> = [];
  documentsUpdatedArray: Array<ProjectCharterDocuments> = [];
  revisionHistoryArray: Array<ProjectCharterRevisionHistory> = [];

  public userEmail;
  public isAdmin = false;
  public isHrManager = false;

  // public submitted = false;
  @Output() successMessage = new EventEmitter<string>();
  @Output() errorMessage = new EventEmitter<string>();

  constructor(
    private formBuilder: FormBuilder,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private rolesManagementService: RolesManagementService,
    private loader: AppLoaderService,
    private globalService: GlobalVariables,
    private projectCharterService: ProjectCharterService,
    private ideaSubmission: IdeaSubmission,
    private projectCharterObj: ProjectCharterEntity,
    private projectCharterResultAreaObj: ProjectCharterResultArea,
    private projectCharterBenefitsObj: ProjectCharterBenefits,
    private projectCharterDmaicphaseTollObj: ProjectCharterDmaicphaseToll,
    private projectCharterSteeringComtProjectTeamObj: ProjectCharterSteeringComtProjectTeam,
    private projectCharterSteeringCommiteeObj: ProjectCharterSteeringCommitee,
    private projectCharterKeyStakeHoldersObj: ProjectCharterKeyStakeHolders,
    private projectCharterDocumentsObj: ProjectCharterDocuments,
    private projectCharterRevisionHistoryObj: ProjectCharterRevisionHistory,) {

    this.loader.open();
    this.today.setDate(this.today.getDate());
    this.userEmail = this.globalService.getUserEmail();
    this.projectIdGenerated = atob(this.activatedRoute.snapshot.queryParams.projectId);
    this.projectTypeId = atob(this.activatedRoute.snapshot.queryParams.projectTypeId);

    this.isAdmin = this.rolesManagementService.isAdmin;
    this.isHrManager = this.rolesManagementService.isHrManager;

    //Lever Technology dropdown Onload
    this.ideaSubmission.getLeverTechnologyDetails().subscribe((data) => {
      this.leverTechnologyList = data["body"];
      this.onLeverTechnologySelect();
    });

    //Lever Technology dropdown Onload
    this.projectCharterService.getDMAICPhaseToll().subscribe((data) => {
      this.DMAICTollGateList = data["body"];
      this.onDMAILTollSelect();
    });

    //Project Type dropdown Onload
    this.projectCharterService.getResultAreaType().subscribe((data) => {
      this.resulAreaNameList = data["body"];
      this.onResultAreaTypeSelect();
    });
  }

  ngOnInit() {
    this.successMessageStatus = this.globalService.getPhraseToShow();
    this.updateProjectCharter = this.formBuilder.group({
      projectName: [""],
      projectSponsor: [""],
      projectManager: [""],
      projectapprovalDate: [""],
      lastrevisionDate: [""],
      problemStatement: [""],
      projectDescription: [""],
      inScope: [""],
      outScope: [""],
      buisnessCase: [""],
      leverId: [""],
      projectDeliverable: [""],
      projectRisk: [""],
      champion: [""],
      masterblackBelt: [""],
      blackBelt: [""],

    });
    this.loader.open();
    this.updateProjectCharter.controls['leverId'].disable();
    this.getProjectCharterDetails();
    this.getProjectCharterResultAreaDetails();
    this.getProjectCharterBenefitsDetails();
    this.getProjectCharterDMAICDetails();
    this.getProjectCharterSteeringDetails();
    this.getProjectCharterSteeringProjectTeamDetails();
    this.getProjectCharterKeyStakeHoldersDetails();
    this.getProjectCharterDocumentsDetails();
    this.getProjectCharterRevisionHistoryDetails();
    this.loader.close();
  }
  onSubmit() {
    this.submitted = true;

    this.loader.open();
    
    this.projectCharterObj.id = Number(this.projectIdGenerated);
    this.projectCharterObj.projecttypeId = this.projectTypeId;
    this.projectCharterObj.projectName = this.updateProjectCharter.value['projectName'];
    this.projectCharterObj.projectSponsor = this.updateProjectCharter.value['projectSponsor'];
    this.projectCharterObj.projectManager = this.updateProjectCharter.value['projectManager'];
    this.projectCharterObj.projectapprovalDate = this.updateProjectCharter.value['projectapprovalDate'];
    this.projectCharterObj.lastrevisionDate = this.updateProjectCharter.value['lastrevisionDate'];
    this.projectCharterObj.problemStatement = this.updateProjectCharter.value['problemStatement'];
    this.projectCharterObj.inScope = this.updateProjectCharter.value['inScope'];
    this.projectCharterObj.outScope = this.updateProjectCharter.value['outScope'];
    this.projectCharterObj.projectDeliverable = this.updateProjectCharter.value['projectDeliverable'];
    this.projectCharterObj.projectDescription = this.updateProjectCharter.value['projectDescription'];
    this.projectCharterObj.buisnessCase = this.updateProjectCharter.value['buisnessCase'];
    this.projectCharterObj.projectRisk = this.updateProjectCharter.value['projectRisk'];
    this.projectCharterObj.leverId = this.updateProjectCharter.value['leverId'];
    this.projectCharterObj.createdBy = this.userEmail;
    this.projectCharterService.UpdateProjectCharterDetails(this.projectCharterObj).subscribe((data) => {

      this.projectCharterSteeringCommiteeObj.projectId = Number(this.projectIdGenerated);
      this.projectCharterSteeringCommiteeObj.projecttypeId = Number(this.projectTypeId);
      this.projectCharterSteeringCommiteeObj.champion = this.updateProjectCharter.value['champion'];
      this.projectCharterSteeringCommiteeObj.blackBelt = this.updateProjectCharter.value['blackBelt'];
      this.projectCharterSteeringCommiteeObj.masterblackBelt = this.updateProjectCharter.value['masterblackBelt'];
      this.projectCharterSteeringCommiteeObj.createdBy = this.userEmail;

      this.projectCharterService.InsertProjectCharterSteeringCommittedetail(this.projectCharterSteeringCommiteeObj, Number(this.projectIdGenerated), Number(this.projectTypeId)).subscribe((data) => {
      });

      this.projectCharterService.InsertProjectCharterResultArea(this.blankResultAreaArray, Number(this.projectIdGenerated), Number(this.projectTypeId)).subscribe((data) => {
      });
      this.projectCharterService.InsertProjectCharterDmaicphaseToll(this.dmailTollPhaseArray, Number(this.projectIdGenerated), Number(this.projectTypeId)).subscribe((data) => {
      });

      this.projectCharterService.InsertProjectCharterSteeringCommitteTeamdetail(this.steeringcomiteeTeamArray, Number(this.projectIdGenerated), Number(this.projectTypeId)).subscribe((data) => {

      });
      this.projectCharterService.InsertProjectCharterBenefitDetail(this.blankBenefitsArray, Number(this.projectIdGenerated), Number(this.projectTypeId)).subscribe((data) => {
      });
     
      this.projectCharterService.InsertProjectCharterKeyStakeHolderdetail(this.keyStakeHoldersArray, Number(this.projectIdGenerated), Number(this.projectTypeId)).subscribe((data) => {
      });
      this.projectCharterService.InsertProjectCharterDocumentDetail(this.documentsUpdatedArray, Number(this.projectIdGenerated), Number(this.projectTypeId)).subscribe((data) => {
      });
      this.projectCharterService.InsertProjectCharterRevisionHistorydetail(this.revisionHistoryArray, Number(this.projectIdGenerated), Number(this.projectTypeId)).subscribe((data) => {

        if (data["status"] === true) {
          this.successMessage.emit("Project Charter details updated successfully.");
          this.router.navigate(["../submittedProjectCharter"], {
            relativeTo: this.activatedRoute,
          });

        }
        this.loader.close();
      }, (error) => {
        this.errorMessage.emit("Failed to update project charter details");
        this.loader.close();
      });
    });
  }

  getProjectCharterDetails() {
    this.projectCharterService
      .getProjectCharterByProjectIdDetails(this.projectIdGenerated)
      .subscribe((response) => {
        if (Utils.isValidInput(response["body"])) {
           this.updateProjectCharter.patchValue(response["body"][0]);
        }
        else {
          this.errorMessage.emit("No Record Found..!!");
        }
      });
  }

  getProjectCharterResultAreaDetails() {
    this.projectCharterService
      .getProjectCharterResultAreaDetails(this.projectIdGenerated, this.projectTypeId)
      .subscribe((response) => {

        if (response["body"] != null && response["body"].length > 0) {

          this.projectCharterResultAreaObj.projectId = response["body"][0].projectId;
          this.projectCharterResultAreaObj.projecttypeId = response["body"][0].projecttypeId;
          this.projectCharterResultAreaObj.resultareaId = response["body"][0].resultareaId;
          this.projectCharterResultAreaObj.description = response["body"][0].description;
          // this.projectCharterResultAreaObj.description = this.userEmail;

          this.blankResultAreaArray.push(this.projectCharterResultAreaObj);
        }
      });
  }

  getProjectCharterBenefitsDetails() {
    this.projectCharterService
      .getProjectCharterBenefitsDetails(this.projectIdGenerated, this.projectTypeId)
      .subscribe((response) => {
        if (response["body"] != null && response["body"].length > 0) {

          this.projectCharterBenefitsObj.projectId = response["body"][0].projectId;
          this.projectCharterBenefitsObj.projecttypeId = response["body"][0].projecttypeId;
          this.projectCharterBenefitsObj.kpi = response["body"][0].kpi;
          this.projectCharterBenefitsObj.baseline = response["body"][0].baseline;
          this.projectCharterBenefitsObj.goal = response["body"][0].goal;
          // this.projectCharterBenefitsObj.goal = this.userEmail;

          this.blankBenefitsArray.push(this.projectCharterBenefitsObj);
        }
      });
  }

  getProjectCharterDMAICDetails() {
    this.projectCharterService
      .getProjectCharterDMAICDetails(this.projectIdGenerated, this.projectTypeId)
      .subscribe((response) => {
        if (response["body"] != null && response["body"].length > 0) {

          this.projectCharterDmaicphaseTollObj.projectId = response["body"][0].projectId;
          this.projectCharterDmaicphaseTollObj.projecttypeId = response["body"][0].projecttypeId;
          this.projectCharterDmaicphaseTollObj.dmaicId = response["body"][0].dmaicId;
          this.projectCharterDmaicphaseTollObj.startDate = response["body"][0].startDate;
          this.projectCharterDmaicphaseTollObj.endDate = response["body"][0].endDate;
          this.projectCharterDmaicphaseTollObj.tollgatereviewDate = response["body"][0].tollgatereviewDate;
          this.projectCharterDmaicphaseTollObj.createdBy = this.userEmail;

          this.dmailTollPhaseArray.push(this.projectCharterDmaicphaseTollObj);
        }
      });
  }

  getProjectCharterSteeringDetails() {
    this.projectCharterService
      .getProjectSteeringCommiteeDetails(this.projectIdGenerated, this.projectTypeId)
      .subscribe((response) => {
        if (response["body"] != null && response["body"].length > 0) {
          this.updateProjectCharter.patchValue(response["body"][0]);
        }
      });
  }

  getProjectCharterSteeringProjectTeamDetails() {
    this.projectCharterService
      .getProjectCharterSteeringTeamDetails(this.projectIdGenerated, this.projectTypeId)
      .subscribe((response) => {
        if (response["body"] != null && response["body"].length > 0) {

          this.projectCharterSteeringComtProjectTeamObj.projectId = response["body"][0].projectId;
          this.projectCharterSteeringComtProjectTeamObj.projecttypeId = response["body"][0].projecttypeId;
          this.projectCharterSteeringComtProjectTeamObj.employeeName = response["body"][0].employeeName;
          this.projectCharterSteeringComtProjectTeamObj.employeeId = response["body"][0].employeeId;
          this.projectCharterSteeringComtProjectTeamObj.createdBy = this.userEmail;

          this.steeringcomiteeTeamArray.push(this.projectCharterSteeringComtProjectTeamObj);
        }
      });
  }

  getProjectCharterKeyStakeHoldersDetails() {
    this.projectCharterService
      .getProjectCharterKeyStakeHoldersDetails(this.projectIdGenerated, this.projectTypeId)
      .subscribe((response) => {
        if (response["body"] != null && response["body"].length > 0) {

          this.projectCharterKeyStakeHoldersObj.projectId = response["body"][0].projectId;
          this.projectCharterKeyStakeHoldersObj.projecttypeId = response["body"][0].projecttypeId;
          this.projectCharterKeyStakeHoldersObj.ksName = response["body"][0].ksName;
          this.projectCharterKeyStakeHoldersObj.successCriteria = response["body"][0].successCriteria;
          this.projectCharterKeyStakeHoldersObj.createdBy = this.userEmail;

          this.keyStakeHoldersArray.push(this.projectCharterKeyStakeHoldersObj);
        }
      });
  }

  getProjectCharterDocumentsDetails() {
    this.projectCharterService
      .getProjectCharterDocumentsDetails(this.projectIdGenerated, this.projectTypeId)
      .subscribe((response) => {
        if (response["body"] != null && response["body"].length > 0) {

          this.projectCharterDocumentsObj.projectId = response["body"][0].projectId;
          this.projectCharterDocumentsObj.projecttypeId = response["body"][0].projecttypeId;
          this.projectCharterDocumentsObj.documentName = response["body"][0].documentName;
          this.projectCharterDocumentsObj.documentNo = response["body"][0].documentNo;
          this.projectCharterDocumentsObj.resp = response["body"][0].resp;
          this.projectCharterDocumentsObj.requestedOn = response["body"][0].requestedOn;
          this.projectCharterDocumentsObj.updatedOn = response["body"][0].updatedOn;
          this.projectCharterDocumentsObj.respRepository = response["body"][0].respRepository;
          this.projectCharterDocumentsObj.createdBy = this.userEmail;

          this.documentsUpdatedArray.push(this.projectCharterDocumentsObj);
        }
      });
  }

  getProjectCharterRevisionHistoryDetails() {
    this.projectCharterService
      .getProjectCharterRevisionHistoryDetails(this.projectIdGenerated, this.projectTypeId)
      .subscribe((response) => {
        if (response["body"] != null && response["body"].length > 0) {

          this.projectCharterRevisionHistoryObj.projectId = response["body"][0].projectId;
          this.projectCharterRevisionHistoryObj.projecttypeId = response["body"][0].projecttypeId;
          this.projectCharterRevisionHistoryObj.description = response["body"][0].description;
          this.projectCharterRevisionHistoryObj.dateModified = response["body"][0].dateModified;
          this.projectCharterRevisionHistoryObj.checkedBy = response["body"][0].checkedBy;
          this.projectCharterRevisionHistoryObj.approvedBy = response["body"][0].approvedBy;
          this.projectCharterRevisionHistoryObj.createdBy = this.userEmail;

          this.revisionHistoryArray.push(this.projectCharterRevisionHistoryObj);
        }
      });
  }


  onDMAILTollSelect() {
    let leverList = this.DMAICTollGateList;
    // let technologyName = _.find(leverList, { 'id': Number(this.insertIdeaSubmission.controls['leverid'].value) }).technologyName;
    // this.insertIdeaSubmission.controls['levertechnologyName'].setValue(technologyName);
  }

  onResultAreaTypeSelect() {
    let projectTypeList = this.resulAreaNameList;
    // let projectTypeName = _.find(projectTypeList, { 'id': Number(this.insertIdeaSubmission.controls['projecttypeid'].value) }).projecttypeName;
    // this.insertIdeaSubmission.controls['projecttypeName'].setValue(projectTypeName);
  }

  onLeverTechnologySelect() {
    let leverList = this.leverTechnologyList;
    // let technologyName = _.find(leverList, { 'id': Number(this.insertIdeaSubmission.controls['leverid'].value) }).technologyName;
    // this.insertIdeaSubmission.controls['levertechnologyName'].setValue(technologyName);
  }

  public addBlankRow(type) {
    if (type === 'resultArea') {
      this.addResultAreaDetail();
    } else if (type === 'benefits') {
      this.addBenefitsDetail();
    } else if (type === 'dmailTollPhase') {
      this.addDmaicTollPhaseDetail();
    } else if (type === 'steeringcomiteeTeam') {
      this.addSteeringComitteDetail();
    }
    else if (type === 'keyStakeHolders') {
      this.addKeyStakeHoldersDetail();
    }
    else if (type === 'documentsUpdated') {
      this.addDocumentUpdateDetail();
    }
    else if (type === 'revisionHistory') {
      this.addRevisionHistoryDetail();
    }
  }

  public deleteRow(index, type) {
    if (type === 'resultArea') {
      this.blankResultAreaArray.splice(index, 1);
    } else if (type === 'benefits') {
      this.blankBenefitsArray.splice(index, 1);
    } else if (type === 'dmailTollPhase') {
      this.dmailTollPhaseArray.splice(index, 1);
    } else if (type === 'steeringcomiteeTeam') {
      this.steeringcomiteeTeamArray.splice(index, 1);
    }
    else if (type === 'keyStakeHolders') {
      this.keyStakeHoldersArray.splice(index, 1);
    }
    else if (type === 'documentsUpdated') {
      this.documentsUpdatedArray.splice(index, 1);
    }
    else if (type === 'revisionHistory') {
      this.revisionHistoryArray.splice(index, 1);
    }
  }


  private addResultAreaDetail() {
    const resulAreablankRowData = new ProjectCharterResultArea();
    (resulAreablankRowData.projectId = Number(this.projectIdGenerated)),
      (resulAreablankRowData.projecttypeId = Number(this.projectTypeId)),
      (resulAreablankRowData.resultareaId = 0),
      (resulAreablankRowData.resultareaName = ""),
      (resulAreablankRowData.description = ""),
      (resulAreablankRowData.createdBy = this.userEmail),
      this.blankResultAreaArray.push(resulAreablankRowData);
  }

  private addBenefitsDetail() {
    const benefitsblankRowData = new ProjectCharterBenefits();
    (benefitsblankRowData.projectId = Number(this.projectIdGenerated)),
      (benefitsblankRowData.projecttypeId = Number(this.projectTypeId)),
      (benefitsblankRowData.kpi = ""),
      (benefitsblankRowData.baseline = ""),
      (benefitsblankRowData.goal = ""),
      (benefitsblankRowData.createdBy = this.userEmail),
      this.blankBenefitsArray.push(benefitsblankRowData);
  }

  private addDmaicTollPhaseDetail() {
    const dmailPhasetollblankRowData = new ProjectCharterDmaicphaseToll();
    (dmailPhasetollblankRowData.projectId = Number(this.projectIdGenerated)),
      (dmailPhasetollblankRowData.projecttypeId = Number(this.projectTypeId)),
      (dmailPhasetollblankRowData.dmaicId = 0),
      (dmailPhasetollblankRowData.startDate = ""),
      (dmailPhasetollblankRowData.endDate = ""),
      (dmailPhasetollblankRowData.tollgatereviewDate = ""),
      (dmailPhasetollblankRowData.createdBy = this.userEmail),
      this.dmailTollPhaseArray.push(dmailPhasetollblankRowData);
  }

  private addSteeringComitteDetail() {
    const steeringComTeamDetailArray = new ProjectCharterSteeringComtProjectTeam();
    (steeringComTeamDetailArray.projectId = Number(this.projectIdGenerated)),
      (steeringComTeamDetailArray.projecttypeId = Number(this.projectTypeId)),
      (steeringComTeamDetailArray.employeeId = ""),
      (steeringComTeamDetailArray.employeeName = ""),
      (steeringComTeamDetailArray.createdBy = this.userEmail),
      this.steeringcomiteeTeamArray.push(steeringComTeamDetailArray);
  }

  private addKeyStakeHoldersDetail() {
    const keyStakeHoldersblankRowData = new ProjectCharterKeyStakeHolders();
    (keyStakeHoldersblankRowData.projectId = Number(this.projectIdGenerated)),
      (keyStakeHoldersblankRowData.projecttypeId = Number(this.projectTypeId)),
      (keyStakeHoldersblankRowData.ksName = ""),
      (keyStakeHoldersblankRowData.successCriteria = ""),
      (keyStakeHoldersblankRowData.createdBy = this.userEmail),
      this.keyStakeHoldersArray.push(keyStakeHoldersblankRowData);
  }

  private addDocumentUpdateDetail() {
    const documentsblankRowData = new ProjectCharterDocuments();
    (documentsblankRowData.projectId = Number(this.projectIdGenerated)),
      (documentsblankRowData.projecttypeId = Number(this.projectTypeId)),
      (documentsblankRowData.documentName = ""),
      (documentsblankRowData.documentNo = ""),
      (documentsblankRowData.resp = ""),
      (documentsblankRowData.requestedOn = ""),
      (documentsblankRowData.updatedOn = ""),
      (documentsblankRowData.respRepository = ""),
      (documentsblankRowData.createdBy = this.userEmail),
      this.documentsUpdatedArray.push(documentsblankRowData);
  }

  private addRevisionHistoryDetail() {
    const revisionHisblankRowData = new ProjectCharterRevisionHistory();
    (revisionHisblankRowData.projectId = Number(this.projectIdGenerated)),
      (revisionHisblankRowData.projecttypeId = Number(this.projectTypeId)),
      (revisionHisblankRowData.description = ""),
      (revisionHisblankRowData.dateModified = ""),
      (revisionHisblankRowData.checkedBy = ""),
      (revisionHisblankRowData.approvedBy = ""),
      (revisionHisblankRowData.createdBy = this.userEmail),
      this.revisionHistoryArray.push(revisionHisblankRowData);
  }

  checkInteger(event) {
    return (event.charCode == 8 || event.charCode == 0 || event.charCode == 13) ? null : event.charCode >= 48 && event.charCode <= 57
  }

  getToday(): string {
    return new Date().toISOString().split('T')[0]
  }

  validateEmail(event) {
    var reg = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
    if (reg.test(event.target.value) == false) {
      alert('Please Enter Valid Email Address');
      event.target.value = "";
      return false;
    }
    return true;
  }

}

