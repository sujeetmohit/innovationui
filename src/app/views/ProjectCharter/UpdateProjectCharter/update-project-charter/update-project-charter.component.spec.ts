import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateProjectCharterComponent } from './update-project-charter.component';

describe('UpdateProjectCharterComponent', () => {
  let component: UpdateProjectCharterComponent;
  let fixture: ComponentFixture<UpdateProjectCharterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateProjectCharterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateProjectCharterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
