import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewProjectCharterComponent } from './view-project-charter.component';

describe('ViewProjectCharterComponent', () => {
  let component: ViewProjectCharterComponent;
  let fixture: ComponentFixture<ViewProjectCharterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewProjectCharterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewProjectCharterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
