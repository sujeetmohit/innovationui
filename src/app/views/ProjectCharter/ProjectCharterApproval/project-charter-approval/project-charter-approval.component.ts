import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';
import { MAT_DATE_LOCALE, DateAdapter, MAT_DATE_FORMATS } from '@angular/material/core';
// import { MomentDateAdapter } from '@angular/material-moment-adapter';
import { ImageService, HtmlEditorService, TableService, ToolbarService, LinkService } from '@syncfusion/ej2-angular-richtexteditor';
import Utils from '../../../../Services/utility/utils';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { RolesManagementService } from '../../../../Services/Roles Service/roles-management.service';
import { AppLoaderService } from '../../../../containers/app-loader/app-loader.service';
import { GlobalVariables } from '../../../../Services/utility/globalVariables';
import { ProjectCharterService } from '../../../../Services/ProjectCharter/projectCharterService';
import { IdeaSubmission } from '../../../../Services/IdeaSubmission/ideasubmissionservice';
import { ProjectCharterEntity } from '../../../../Modals/ProjectCharter/projectCharter';

export const MY_DATE_FORMATS = {
  parse: {
    dateInput: 'DD/MM/YYYY',
  },
  display: {
    dateInput: 'DD/MM/YYYY',
    monthYearLabel: 'MMMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY'
  },
};

@Component({
  selector: 'app-project-charter-approval',
  templateUrl: './project-charter-approval.component.html',
  styleUrls: ['./project-charter-approval.component.css'],

  providers: [{ provide: MAT_DATE_LOCALE, useValue: "en-US" },
  // {
  //   provide: DateAdapter,
  //   useClass: MomentDateAdapter,
  //   deps: [MAT_DATE_LOCALE],
  // },
  { provide: MAT_DATE_FORMATS, useValue: MY_DATE_FORMATS },
    ToolbarService, LinkService, ImageService, HtmlEditorService, TableService],
})
export class ProjectCharterApprovalComponent implements OnInit {

  @Input() max: any;

  hiddenField = false;
  today = new Date();
  allowedExtensions = ['doc', 'docx', 'pdf', 'msg', 'pst', 'edb', 'ost', 'eml', 'mbox'];
  maxSize = 5000000;
  projectTypeId: string;
  projectIdGenerated: string;
  approveProjectCharter: FormGroup;
  public errorMessageStatus = '';
  public successMessageStatus = '';
  submitted = false;
  DMAICTollGateList = new Array();
  leverTechnologyList = new Array();
  resulAreaNameList = new Array();

  public userEmail;
  public isAdmin = false;
  public isHrManager = false;

  // public submitted = false;
  @Output() successMessage = new EventEmitter<string>();
  @Output() errorMessage = new EventEmitter<string>();

  constructor(
    private formBuilder: FormBuilder,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private rolesManagementService: RolesManagementService,
    private loader: AppLoaderService,
    private globalService: GlobalVariables,
    private projectCharterService: ProjectCharterService,
    private ideaSubmission: IdeaSubmission,
    private projectCharterObj: ProjectCharterEntity,
  ) {

    this.loader.open();
    this.today.setDate(this.today.getDate());
    this.userEmail = this.globalService.getUserEmail();
    this.projectIdGenerated = atob(this.activatedRoute.snapshot.queryParams.projectId);
    this.projectTypeId = atob(this.activatedRoute.snapshot.queryParams.projectTypeId);

    this.isAdmin = this.rolesManagementService.isAdmin;
    this.isHrManager = this.rolesManagementService.isHrManager;

    //Lever Technology dropdown Onload
    this.ideaSubmission.getLeverTechnologyDetails().subscribe((data) => {
      this.leverTechnologyList = data["body"];
      this.onLeverTechnologySelect();
      this.disableControl();
    });
  }

  ngOnInit(): void {
    this.successMessageStatus = this.globalService.getPhraseToShow();
    this.approveProjectCharter = this.formBuilder.group({
      projectName: [""],
      projectSponsor: [""],
      projectManager: [""],
      projectapprovalDate: [""],
      lastrevisionDate: [""],
      problemStatement: [""],
      projectDescription: [""],
      inScope: [""],
      outScope: [""],
      buisnessCase: [""],
      leverId: [""],
      projectDeliverable: [""],
      projectRisk: [""],
      champion: [""],
      masterblackBelt: [""],
      blackBelt: [""],
      comments: [""],
    });
    this.approveProjectCharter.controls['leverId'].disable();
    this.getProjectCharterDetails();
    this.loader.close();
  }

  onSubmit() {
  }

  getProjectCharterDetails() {
    this.projectCharterService
      .getProjectCharterByProjectIdDetails(this.projectIdGenerated)
      .subscribe((response) => {
        if (Utils.isValidInput(response["body"])) {
          this.approveProjectCharter.patchValue(response["body"][0]);
        }
        else {
          this.errorMessage.emit("No Record Found..!!");
        }
      });
  }

  onLeverTechnologySelect() {
    let leverList = this.leverTechnologyList;
    // let technologyName = _.find(leverList, { 'id': Number(this.insertIdeaSubmission.controls['leverid'].value) }).technologyName;
    // this.insertIdeaSubmission.controls['levertechnologyName'].setValue(technologyName);
  }

  onApproveClick() {
    this.loader.open();
    this.errorMessageStatus = '';
    //  Status 1 shows approval
    this.projectCharterService.UpdateProjectCharterStatus(this.projectIdGenerated, this.userEmail, this.approveProjectCharter.controls['comments'].value, 1).subscribe((reponse) => {
      if (reponse["status"] === true) {
        this.globalService.setPhraseToShow("Project Charter Status Updated Successfully.");
      }
      this.loader.close();
      this.router.navigate(["../submittedProjectCharter"], { relativeTo: this.activatedRoute });
    }, (error) => {
      this.errorMessageStatus = 'Failed to update the Project Charter Status';
      this.loader.close();
    });
  }

  onRejectClick() {
    this.loader.open();
    this.errorMessageStatus = '';
    //  Status 2 shows reject
    this.projectCharterService.UpdateProjectCharterStatus(this.projectIdGenerated, this.userEmail, this.approveProjectCharter.controls['comments'].value, 2).subscribe((reponse) => {
      if (reponse["status"] === true) {
        this.globalService.setPhraseToShow("Project Charter Status Updated Successfully.");
      }
      this.loader.close();
      this.router.navigate(["../submittedProjectCharter"], { relativeTo: this.activatedRoute });
    }, (error) => {
      this.errorMessageStatus = 'Failed to update the Project Charter Status';
      this.loader.close();
    });
  }

  onReturnClick() {
    this.loader.open();
    this.errorMessageStatus = '';
    //  Status 5 shows Return
    this.projectCharterService.UpdateProjectCharterStatus(this.projectIdGenerated, this.userEmail, this.approveProjectCharter.controls['comments'].value, 5).subscribe((reponse) => {
      if (reponse["status"] === true) {
        this.globalService.setPhraseToShow("Project Charter Status Updated Successfully.");
      }
      this.loader.close();
      this.router.navigate(["../submittedProjectCharter"], { relativeTo: this.activatedRoute });
    }, (error) => {
      this.errorMessageStatus = 'Failed to update the Project Charter Status';
      this.loader.close();
    });
  }

  disableControl() {
    this.approveProjectCharter.controls['projectName'].disable();
    this.approveProjectCharter.controls['projectSponsor'].disable();
    this.approveProjectCharter.controls['projectManager'].disable();
    this.approveProjectCharter.controls['projectapprovalDate'].disable();
    this.approveProjectCharter.controls['lastrevisionDate'].disable();
    this.approveProjectCharter.controls['problemStatement'].disable();
    this.approveProjectCharter.controls['projectDescription'].disable();
    this.approveProjectCharter.controls['inScope'].disable();
    this.approveProjectCharter.controls['outScope'].disable();
    this.approveProjectCharter.controls['buisnessCase'].disable();
    this.approveProjectCharter.controls['leverId'].disable();
    this.approveProjectCharter.controls['projectDeliverable'].disable();
    this.approveProjectCharter.controls['projectRisk'].disable();
  }

}
