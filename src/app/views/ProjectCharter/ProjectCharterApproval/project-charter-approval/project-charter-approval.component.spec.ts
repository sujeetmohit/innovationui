import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProjectCharterApprovalComponent } from './project-charter-approval.component';

describe('ProjectCharterApprovalComponent', () => {
  let component: ProjectCharterApprovalComponent;
  let fixture: ComponentFixture<ProjectCharterApprovalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProjectCharterApprovalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjectCharterApprovalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
