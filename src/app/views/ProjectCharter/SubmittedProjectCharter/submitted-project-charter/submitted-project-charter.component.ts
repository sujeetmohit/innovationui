import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { IdeaSubmission } from '../../../../Services/IdeaSubmission/ideasubmissionservice';
import { RolesManagementService } from '../../../../Services/Roles Service/roles-management.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { DatePipe } from '@angular/common';
import { AppLoaderService } from '../../../../containers/app-loader/app-loader.service';
import { GlobalVariables } from '../../../../Services/utility/globalVariables';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { ProjectCharterService } from '../../../../Services/ProjectCharter/projectCharterService';

@Component({
  selector: 'app-submitted-project-charter',
  templateUrl: './submitted-project-charter.component.html',
  styleUrls: ['./submitted-project-charter.component.css']
})
export class SubmittedProjectCharterComponent implements OnInit {

  hiddenField: boolean = false;
  projectCharterList: ProjectCharterList;
  userName: string = '';
  sameloginUser: boolean = false


  displayedColumns: string[] = [
    "id",
    "projectName",
    "projecttypeName",
    "projectcharterstatusName",
    "projectSponsor",
    "inScope",
    "outScope",
    "action",
  ];

  dataSource: MatTableDataSource<ProjectCharterList>;

  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  public errorMessageStatus = '';
  public successMessageStatus = '';
  public userEmail;
  public isAdmin = false;
  public isHrManager = false;
  public isManager = false;
  public isBTAdmin = false;
  public isAssociate = false;

  constructor(
    private formBuilder: FormBuilder,
    private activatedRoute: ActivatedRoute,
    public datepipe: DatePipe,
    private router: Router,
    private rolesManagementService: RolesManagementService,
    private loader: AppLoaderService,
    private globalService: GlobalVariables,
    private ideaSubmission: IdeaSubmission,
    private projectCharterService: ProjectCharterService,) {

    this.loader.open();
    this.userName = this.globalService.getUserEmail();
    this.isAdmin = this.rolesManagementService.isAdmin;
    this.isHrManager = this.rolesManagementService.isHrManager;
    this.isManager = this.rolesManagementService.isManager;
    this.isBTAdmin = this.rolesManagementService.isBTAdmin;
    this.isAssociate = this.rolesManagementService.isAssociate;
  }

  ngOnInit() {
    this.getSubmittedProjectCharter();
  }

  getSubmittedProjectCharter() {
    this.loader.open();
    this.projectCharterService.getProjectCharterDetails().subscribe((data) => {
      this.dataSource = new MatTableDataSource(data["body"]);
      this.projectCharterList = data["body"];
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
      if (this.dataSource.data.length === 0) {
        this.hiddenField = true;
      } else {
        this.hiddenField = false;
      }
      this.loader.close();
    }, (error) => {
      this.loader.close();
    });
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  updateProjectCharter(rowData: any) {
    this.loader.open();
    this.router.navigate(["../updateProjectCharter"], {
      queryParams: { projectId: btoa(rowData.id), projectTypeId: btoa(rowData.projecttypeId) },
      relativeTo: this.activatedRoute,
    });
  }

  approveProjectCharter(rowData: any) {
    this.loader.open();
    this.router.navigate(["../projectCharterApproval"], {
      queryParams: { projectId: btoa(rowData.id), projectTypeId: btoa(rowData.projecttypeId) },
      relativeTo: this.activatedRoute,
    });
  }

}
export interface ProjectCharterList {
  projectName: string;
  projecttypeName: string;
  projectcharterstatusName: string;
  projectSponsor: string;
  inScope: string;
  outScope: string;
}
