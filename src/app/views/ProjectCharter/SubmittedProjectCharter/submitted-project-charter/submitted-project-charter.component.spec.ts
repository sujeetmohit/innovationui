import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubmittedProjectCharterComponent } from './submitted-project-charter.component';

describe('SubmittedProjectCharterComponent', () => {
  let component: SubmittedProjectCharterComponent;
  let fixture: ComponentFixture<SubmittedProjectCharterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubmittedProjectCharterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubmittedProjectCharterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
