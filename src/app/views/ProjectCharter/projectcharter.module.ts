import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { DatePipe, CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { MatNativeDateModule, MAT_DATE_LOCALE } from '@angular/material/core';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatBadgeModule } from '@angular/material/badge';
import { MatButtonModule } from '@angular/material/button';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatCardModule } from '@angular/material/card';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatChipsModule } from '@angular/material/chips';
import { MatStepperModule } from '@angular/material/stepper';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatDialogModule } from '@angular/material/dialog';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatListModule } from '@angular/material/list';
import { MatMenuModule } from '@angular/material/menu';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatRadioModule } from '@angular/material/radio';
import { MatRippleModule } from '@angular/material/core';
import { MatSelectModule } from '@angular/material/select';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatSliderModule } from '@angular/material/slider';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatSortModule } from '@angular/material/sort';
import { MatTableModule } from '@angular/material/table';
import { MatTabsModule } from '@angular/material/tabs';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatTreeModule } from '@angular/material/tree';
import { RichTextEditorAllModule } from '@syncfusion/ej2-angular-richtexteditor';
import { SharedModule } from '../../containers/shared.module';
import { RouterModule } from '@angular/router';
import { routes } from '../../app.routing';
import { ProjectCharterRoutingModule } from './projectcharter-routing';
import { ProjectCharterService } from '../../Services/ProjectCharter/projectCharterService';
import { NewProjectCharterComponent } from './NewProjectCharter/new-project-charter/new-project-charter.component';
import { UpdateProjectCharterComponent } from './UpdateProjectCharter/update-project-charter/update-project-charter.component';
import { ViewProjectCharterComponent } from './ViewProjectCharter/view-project-charter/view-project-charter.component';
import { ProjectCharterEntity, ProjectCharterResultArea, ProjectCharterSteeringCommitee, ProjectCharterKeyStakeHolders, ProjectCharterDmaicphaseToll, ProjectCharterRevisionHistory, ProjectCharterDocuments, ProjectCharterSteeringComtProjectTeam, ProjectCharterBenefits } from '../../Modals/ProjectCharter/projectCharter';
import { SubmittedProjectCharterComponent } from './SubmittedProjectCharter/submitted-project-charter/submitted-project-charter.component';
import { CreateProjectCharterComponent } from './CreateProjectCharterbyIdea/create-project-charter/create-project-charter.component';
import { ProjectCharterApprovalComponent } from './ProjectCharterApproval/project-charter-approval/project-charter-approval.component';
import { IdeaSubmissionEntity } from '../../Modals/Idea/IdeaSubmission';


@NgModule({
  imports: [ProjectCharterRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    MatAutocompleteModule,
    MatBadgeModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatCardModule,
    MatCheckboxModule,
    MatChipsModule,
    MatStepperModule,
    MatDatepickerModule,
    MatDialogModule,
    MatExpansionModule,
    MatFormFieldModule,
    MatGridListModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatPaginatorModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatRippleModule,
    MatSelectModule,
    MatSidenavModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatSnackBarModule,
    MatSortModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule,
    MatTooltipModule,
    MatTreeModule,
    MatNativeDateModule,
    RichTextEditorAllModule,
    SharedModule
  ],
  declarations: [
    NewProjectCharterComponent,
    UpdateProjectCharterComponent,
    ViewProjectCharterComponent,
    SubmittedProjectCharterComponent,
    CreateProjectCharterComponent,
    ProjectCharterApprovalComponent],
  providers: [DatePipe, ProjectCharterService, ProjectCharterEntity, ProjectCharterResultArea,
    ProjectCharterBenefits, ProjectCharterDmaicphaseToll, ProjectCharterSteeringCommitee,
    ProjectCharterSteeringComtProjectTeam, ProjectCharterKeyStakeHolders,
    ProjectCharterDocuments, ProjectCharterRevisionHistory,ProjectCharterSteeringCommitee
  ],
  entryComponents: [],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProjectCharterModule { }
