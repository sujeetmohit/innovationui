import { INavData } from '@coreui/angular';

export const navItems: INavData[] = [
  {
    name: 'Dashboard',
    url: '/dashboard',
    icon: 'icon-speedometer',
    roles: ['manager', 'hr manager', 'recruiter', 'hr', 'associate', 'interviewer', 'finance', 'admin'],
    priority: 1
  },

  {
    name: 'Idea',
    url: '/Idea',
    icon: 'icon-people',
    roles: ['manager', 'hr manager', 'recruiter', 'hr', 'finance', 'admin'],
    priority: 2,

    children: [
      {
        name: 'Submitted',
        url: '/Idea/showIdeaSubmission',
        icon: 'icon-arrow-right',
        roles: ['manager', 'hr manager', 'hr', 'admin']
      }
    ]
  },
  {
    name: 'Candidate Management',
    url: '/Recruitment',
    icon: 'icon-briefcase',
    roles: ['manager', 'hr manager', 'recruiter', 'hr', 'admin'],
    priority: 4,
    children: [
      {
        name: 'New Applicant',
        url: '/Recruitment/new-recruitment',
        icon: 'icon-arrow-right',
        roles: ['recruiter', 'manager', 'hr manager', 'admin'],
      },
      {
        name: 'Show Applicant',
        url: '/Recruitment/show-recruitment',
        icon: 'icon-arrow-right',
        roles: ['manager', 'hr manager', 'recruiter', 'hr', 'admin'],
      },
      {
        name: 'Selected Candidates',
        url: '/Recruitment/selected-candidates',
        icon: 'icon-arrow-right',
        roles: ['hr manager', 'admin'],
      }, {
        name: 'Initiate Joining',
        url: '/Recruitment/selected-applicant',
        icon: 'icon-arrow-right',
        roles: ['hr manager', 'admin'],
      },

    ]
  },

  {
    name: 'Onboarding',
    url: '/Onboarding',
    icon: 'icon-basket',
    roles: ['manager', 'hr manager', 'recruiter', 'hr', 'associate', 'interviewer', 'admin'],
    priority: 5,
    children: [
      {
        name: 'Forms',
        url: '/Onboarding/onboardingForms',
        icon: 'icon-arrow-right',
        roles: ['manager', 'hr manager', 'recruiter', 'hr', 'associate', 'interviewer', 'admin'],
      },
      {
        name: 'Download Forms',
        url: '/Onboarding/download-forms',
        icon: 'icon-arrow-right',
        roles: ['hr manager', 'admin'],
      }
    ]
  },

  {
    name: 'Employee',
    url: '/Employee',
    icon: 'icon-user',
    roles: ['manager', 'hr manager', 'recruiter', 'hr', 'associate', 'interviewer', 'finance', 'admin'],
    priority: 6,
    children: [
      {
        name: 'Employee Details',
        url: '/Employee/view-employee-list',
        icon: 'icon-arrow-right',
        roles: ['manager', 'admin', 'hr manager'],
      },
      {
        name: 'Employee Profile',
        url: '/Employee/employee-profile',
        icon: 'icon-arrow-right',
        roles: ['manager', 'hr manager', 'recruiter', 'hr', 'associate', 'interviewer', 'finance', 'admin'],
      },
      // {
      //   name: 'Insert Employee',
      //   url: '/Employee/insertEmployee',
      //   icon: 'icon-arrow-right',
      //   roles: ['admin'],
      // },     
      {
        name: 'Employee Self Service',
        url: '/Employee/employeeSelfService',
        icon: 'icon-arrow-right',
        roles: ['manager', 'hr manager', 'recruiter', 'hr', 'associate', 'interviewer', 'finance', 'admin'],
      }
      // ,
      // {
      //   name: 'Employee Feedback',
      //   url: '/Employee/employee-feedback',
      //   icon: 'icon-arrow-right',
      //   roles: ['manager','hr manager','recruiter','hr','associate','interviewer','finance','admin'],
      // }
    ]
  },
  {
    name: 'Reports',
    url: '/Report',
    icon: 'icon-speedometer',
    roles: ['hr manager', 'admin'],
    priority: 8,
    children: [
      {
        name: 'Requisition',
        url: '/Report/requisition',
        icon: 'icon-arrow-right',
        roles: ['hr manager', 'admin'],
      }
    ]
  },
  {
    name: 'Manage Master Data',
    url: '/master',
    icon: 'icon-speedometer',
    roles: ['admin', 'hr manager'],
    priority: 9,
    children: [
      {
        name: 'Departments',
        url: '/master/department',
        icon: 'icon-arrow-right',
        roles: ['admin', 'hr manager'],
      },
      {
        name: 'Designation',
        url: '/master/designation',
        icon: 'icon-arrow-right',
        roles: ['admin', 'hr manager'],
      }
    ]
  }
];
