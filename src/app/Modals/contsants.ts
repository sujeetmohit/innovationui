export const recruitment = {
    addAPI: 'recruitment/addupdateapplicant',
    jobSource: 'recruitment/getjobsource',
    jobSourceDetails: 'recruitment/getjobsourcedetails',
    getApplicantdetails: 'recruitment/getapplicantdetails',
    getApplicantdetailbyId: 'recruitment/getapplicantbyid',
    addUpdateInterview: 'recruitment/addupdateinterview',
    getApplicantinterview: 'recruitment/getinterview',
    getApplicantinterviewById: 'recruitment/getinterviewbyId',
    getSelectedApplicant: 'recruitment/getSelectedApplicant',
    getSelectedApplicantDetailById: 'recruitment/getSelectedApplicantDetail',
    applicantEmailExistance: 'recruitment/getApplicantEmailExistance'
}

export const api = {
    // baseURL: 'http://localhost:8082/api/'

    hrisprodURL: 'http://10.1.10.138:8080/AISHRMIS-0.0.1-SNAPSHOT/api/',
    baseURL: 'http://10.1.10.138:8080/AISHRMIS-0.0.1-SNAPSHOT/api/',

       baseURLINNOVATION: 'http://localhost:8083/api/'

    //  Prod
    //  baseURLINNOVATION: 'http://10.1.10.138:8080/INNOVATIONLAB-0.0.1-SNAPSHOT/api/'

}

export const Upload = {
    resumeUpload: 'fileupload/uploadresume',
    deleteUpload: 'fileupload/DeleteResume',
    downloadResume: 'fileupload/resume/download',
    replacereqUpload: 'fileupload/replacereq/uploaddoc',
    deleteReplaceReq: 'fileupload/replacereq/deletedoc',
    downloadReplaceReq: 'fileupload/replacereq/downloaddoc',
    downoaduploadeddoc: 'fileupload/downloadFile/fileName',
    downloadofferletter: 'fileupload/downloadOfferLetter/id/',
    pnlsendbacktoFinance: 'fileupload/pnlFile/pnlsendbacktoFinance'
}
export const Common = {
    getCity: 'common/getcity',
    getState: 'common/getstate',
    getCountry: 'common/getcountry',
    getDept: 'common/getWorkDeptName',
    getDesignation: 'common/getDesignationDetail',
    addUpdateDesignation: 'common/addUpdateDesignation',
    addUpdateWorkDept: 'common/addUpdateWorkDept',
    getEmployeeRatingScales: 'common/getEmployeeRatingScales',
    getEmployeePerformanceFactor: 'common/getEmployeePerformanceFactor',
    getPfContribution: 'common/getPfContribution',
    getRolePriority: 'common/getRolePriority'
}

export const Report = {
    getOpenPosition: 'report/getOpenPositions',
    getState: 'report/getstate',
}

export const Dashboard = {
    getDashboardDetails: 'report/getDashBoardDetails',
}

export const Requisition = {
    checkRequisitionNumber: 'requisition/checkRequisitionNumber'
}

export const Employee = {
    OrganizationHierarchy: 'employee/getOrganizationHierarchy'
}


export const offer = {
    insertselectedCandidate: 'offer/insertOfferGenertionDetail',
    getselectedcandidate: 'recruitment/getselectedcandidate',
    getselectedcandidateById: 'recruitment/getselectedcandidatebyId',
    uploadofferdocs: 'offer/upload',
    generateofferletter: 'offer/generateOfferLetter',
    getofferedletter: 'offer/getCandidateById',
    deleteuploadeddoc: 'offer/deleteUploadFile/fileName',
    getuploadeddoc: 'offer/getCandidateDoc',
    previewofferletter: 'offer/offerletterpreview',
    pgetfferletterApprovals: 'offer/GetOfferLetterAprovals',
    checkofferletterisSelected: 'offer/CheckOfferisSelected',
    sendofferlettertoCandidate: 'offer/sendOfferlettertoCandidate',
    acceptofferletter: 'offer/acceptOfferletter',
    offerAcceptedandidate: 'recruitment/getofferacceptedCandidate',

}

export const ProjectCharter = {
    approved: 'Approved',
    rejected: 'Rejected',
    returned: 'Returned',
    total: 'All'
}

export const IdeaSubmission = {
    approved: 'Approved',
    rejected: 'Rejected',
    returned: 'Returned',
    total: 'All'
}