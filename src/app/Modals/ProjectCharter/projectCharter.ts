export class ProjectCharterEntity {
    id: Number;
    projecttypeId: string = '';
    projecttypeName: string = "";
    projectName: string = "";
    ProjectcharterstatusId: number = 0;
    projectcharterstatusName: string = "";
    projectcharterstatusUpdatedby: string = "";
    statusupdatedDate: string = "";
    projectSponsor: string = '';
    projectManager: string = '';
    projectapprovalDate: string = '';
    lastrevisionDate: string = '';
    problemStatement: string = '';
    projectDescription: string = '';
    inScope: string = '';
    outScope: string = '';
    leverId: number = 0;
    levertechName: string = '';
    projectDeliverable: string = '';
    buisnessCase: string = '';
    projectRisk: string = '';
    comments: string = '';
    createdBy: string = '';
    updatedby: string = '';
    createddate: string = '';
    updatedate: string = '';
  ideasubmissionId: any;

}

export class ProjectCharterResultArea {
    Id: number = 0;
    projectId: number = 0;
    projecttypeId: number = 0;
    resultareaId: number = 0;
    resultareaName: string = '';
    description: string = '';
    createdBy: string = ''
}

export class ProjectCharterBenefits {
    Id: number = 0;
    projectId: number = 0;
    projecttypeId: number = 0;
    kpi: string = '';
    baseline: string = '';
    goal: string = '';
    createdBy: string = '';
}

export class ProjectCharterDmaicphaseToll {
    Id: number = 0;
    projectId: number = 0;
    projecttypeId: number = 0;
    dmaicId: number = 0;
    dmailphaseName: string = '';
    startDate: string = '';
    endDate: string = '';
    tollgatereviewDate: string = '';
    createdBy: string = '';
    updatedBy: string = '';
}

export class ProjectCharterSteeringCommitee {
    Id: number = 0;
    projectId: number = 0;
    projecttypeId: number = 0;
    champion: string = '';
    masterblackBelt: string = '';
    blackBelt: string = '';
    createdBy: string = '';
    updatedBy: string = '';
}

export class ProjectCharterSteeringComtProjectTeam {
    Id: number = 0;
    projectId: number = 0;
    projecttypeId: number = 0;
    employeeId: string = '';
    employeeName: string = '';
    steeringId: number = 0;
    createdBy: string = '';
    updatedBy: string = '';
}

export class ProjectCharterKeyStakeHolders {
    Id: number = 0;
    projectId: number = 0;
    projecttypeId: number = 0;
    ksName: string = '';
    successCriteria: string = '';
    createdBy: string = '';
    updatedBy: string = '';

}

export class ProjectCharterDocuments {
    Id: number = 0;
    projectId: number = 0;
    projecttypeId: number = 0;
    documentName: string = '';
    documentNo: string = '';
    resp: string = '';
    requestedOn: string = '';
    updatedOn: string = '';
    respRepository: string = '';
    createdBy: string = '';
    updatedBy: string = '';
}

export class ProjectCharterRevisionHistory {
    Id: number = 0;
    projectId: number = 0;
    projecttypeId: number = 0;
    description: string = '';
    dateModified: string = '';
    checkedBy: string = '';
    approvedBy: string = '';
    createdBy: string = '';
    updatedBy: string = '';

}