

export class IdeaSubmission{
  employeeNo: [""];
  fullName: [""];
  designationName: [""];
  clientName: [""];
  locationId: [""];
  locationName: [""];
  workGroupDeptName: [""];

  ownerofficialemail: string = "";
  ideaownername: string = '';
  projectstatusid: string = '';
  designationname: string = '';
  designationid: string = '';
  client: string = '';
  departmentname: string = '';
  departmentid: string = '';
  hod: string = '';
  locationname: string = '';
  locationid: string = '';
  ideaproposalname: string = '';
  problemstatement: string = '';
  riskdescription: string = '';
  benefits: string = '';
  projectcommencementdate: string = '';
  projectcompletiondate: string = '';
  projecttypeid: string = '';
  managerfeedback: string = '';
  receiptfilename: string = '';
  projectstatusidbymanager: string = '';
  projectstatusidbybtteam: string = '';
  btteamapprovalemail: string = '';
  createdby: string = '';
  updateby: string = '';
  createddate: '';
  updatedate: '';





}
