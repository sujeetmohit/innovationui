import { environment } from './../environments/environment';
import { GlobalVariables } from './Services/utility/globalVariables';
import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import Utils from './Services/utility/utils';
import { Title } from '@angular/platform-browser';

@Component({
  // tslint:disable-next-line
  selector: 'body',
  template: '<router-outlet></router-outlet> ',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  constructor(private router: Router,
    private translateService: TranslateService,
    private globalService: GlobalVariables,
    private title:Title) {
    this.translateService.setDefaultLang('en');
    this.title.setTitle('InnovationLab '+environment.appVersion);
    if (!Utils.isValidInput(this.globalService.getToken())) {
      this.router.navigate(['login']);
    }

  }

  ngOnInit() {
    this.router.events.subscribe((evt) => {
      if (!(evt instanceof NavigationEnd)) {
        return;
      }
      window.scrollTo(0, 0);
    });
  }
}
