import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Import Containers
import { DefaultLayoutComponent } from './containers';
import { P404Component } from './views/error/404.component';
import { P500Component } from './views/error/500.component';
import { LoginComponent } from './views/login/login.component';
import { RegisterComponent } from './views/register/register.component';
import { HttpErrorPageComponent } from './views/error/http-error-page/http-error-page.component';
export const routes: Routes = [

  {
    path: '',
    component: LoginComponent,
    data: {
      title: 'Login Page'
    }
  },

  {
    path: 'login',
    component: LoginComponent,
    data: {
      title: 'Login Page'
    }
  },
  {
    path: 'dashboard',
    redirectTo: 'dashboard',
    pathMatch: 'full',
  },
  {
    path: '404',
    component: P404Component,
    data: {
      title: 'Page 404'
    }
  },
  {
    path: '500',
    component: P500Component,
    data: {
      title: 'Page 500'
    }
  },
  {
    path: 'httpError',
    component: HttpErrorPageComponent,
    data: {
      title: 'Page HttpError'
    }
  },

  {
    path: 'register',
    component: RegisterComponent,
    data: {
      title: 'Register Page'
    }
  },
  {
    path: '',
    component: DefaultLayoutComponent,
    data: {
      title: 'Home'
    },
    children: [
      {
        path: 'base',
        loadChildren: () => import('./views/base/base.module').then(m => m.BaseModule)
      },

      {
        path: 'dashboard',
        loadChildren: () => import('./views/dashboard/dashboard.module').then(m => m.DashboardModule)
      },
      {
        path: 'Idea',
        loadChildren: () => import('./views/Idea/idea.module').then(m => m.IdeaSubmissionModule)
      },
      {
        path: 'ProjectCharter',
        loadChildren: () => import('./views/ProjectCharter/projectcharter.module').then(m => m.ProjectCharterModule)
      },
      // {
      //   path: 'InventoryMgmt',
      //   loadChildren: () => import('./views/InventoryMgmt/inventory.module').then(m => m.InventoryModule)
      // },
      // {
      //   path: 'Approval',
      //   loadChildren: () => import('./views/Approval/approval.module').then(m => m.ApprovalModule)
      // },
      // {
      //   path: 'Report',
      //   loadChildren: () => import('./views/Report/report.module').then(m => m.ReportModule)
      // },
      // {
      //   path: 'master',
      //   loadChildren: () => import('./views/master-data/master-data.module').then(m => m.MasterDataModule)
      // },
      // {
      //   path: 'Onboarding',
      //   loadChildren: () => import('./views/OnboardingForm/onboarding.module').then(m => m.OnboardingModule)
      // },
      // {
      //   path: 'Settings',
      //   loadChildren: () => import('./views/Settings/settings.module').then(m => m.SettingsModule)
      // }

    ]
  },
  { path: '**', component: P404Component },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
