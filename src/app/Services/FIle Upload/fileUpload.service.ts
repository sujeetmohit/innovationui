import { Injectable } from "@angular/core";
import { HttpClient, HttpRequest, HttpResponse, HttpHeaders, HttpEvent } from '@angular/common/http';
import { Observable } from 'rxjs';
import { retry, catchError } from "rxjs/operators";
import { throwError } from "rxjs/internal/observable/throwError";
import { api, Upload, offer } from "../../Modals/contsants";
import { off } from 'process';

@Injectable({
    providedIn: "root",
})
export class FileUpload {
    constructor(private httpClient: HttpClient) { }

    uploadResume(id: number, file: File): Observable<any> {
        const formData: FormData = new FormData();

        formData.append('resumefile', file);

        return this.httpClient.post(api.baseURL + Upload.resumeUpload + "?id=" + id, formData, {
            reportProgress: true,
            observe: 'events'
        }).pipe(retry(1), catchError(this.handleError));


    }

    public uploadReplaceReqFile(file: File, reqId) {
        const formData: FormData = new FormData();
        formData.append('replacereqfile', file);
        return this.httpClient.post(api.baseURL + Upload.replacereqUpload + "?replacereqId=" + reqId, formData, {
            reportProgress: true,
            observe: 'events'
        }).pipe(retry(1), catchError(this.handleError));

    }

    public deleteReplaceReqFile(id: number) {
        return this.httpClient
            .get<any>(api.baseURL + Upload.deleteReplaceReq + "?replacereqId=" + id)
            .pipe(retry(1), catchError(this.handleError));

    }

    downloadReplaceReqFile(id: number) {
        return api.baseURL + Upload.downloadReplaceReq + "?replacereqId=" + id;
    }

    public removeResume(path: string, id: number) {
        return this.httpClient
            .get<any>(api.baseURL + Upload.deleteUpload + "?file=" + path + "&id=" + id)
            .pipe(retry(1), catchError(this.handleError));

    }
    downloadFile(id: number) {
        return api.baseURL + Upload.downloadResume + "?id=" + id;
    }
    downloadDocumentsFile(name: string, value: string, id: number) {
        return api.baseURL + Upload.downoaduploadeddoc + "/" + name + "/value/" + value + "/id/" + id
    }

    public uploadCandidateDocs(formData: FormData, id: number) {

        return this.httpClient.post(api.baseURL + offer.uploadofferdocs + "?id=" + id, formData, {
            reportProgress: true,
            observe: 'events'
        }).pipe(retry(1), catchError(this.handleError));

    }
    downloadOfferLetter(id: number) {
        return api.baseURL + Upload.downloadofferletter+id;
    }

    handleError(error) {
        let errorMessage = "";
        if (error.error instanceof ErrorEvent) {
            // client-side error
            errorMessage = `Error: ${error.error.message}`;
        } else {
            // server-side error
            errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
        }
      
        return throwError(errorMessage);
    }
}