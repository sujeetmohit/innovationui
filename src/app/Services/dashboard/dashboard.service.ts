import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { retry, catchError } from "rxjs/operators";
import { throwError } from "rxjs/internal/observable/throwError";
import { api, Dashboard, Common } from '../../Modals/contsants';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {

  baseURL: string = api.baseURLINNOVATION;

  constructor(private httpClient: HttpClient) { }


  public getDashboardDetails() {
    return this.httpClient
      .get<any>(this.baseURL + Dashboard.getDashboardDetails)
      .pipe(retry(1), catchError(this.handleError));
  }

  public getRolePriority(loginEmail) {
    return this.httpClient
      .get<any>(this.baseURL + Common.getRolePriority + '?loginEmail=' + loginEmail)
      .pipe(retry(1), catchError(this.handleError));
  }

  handleError(error) {
    let errorMessage = "";
    if (error.error instanceof ErrorEvent) {
      // client-side error
      errorMessage = `Error: ${error.error.message}`;
    } else {
      // server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    return throwError(errorMessage);
  }
}
