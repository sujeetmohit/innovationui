import { Injectable } from "@angular/core";
import { GlobalVariables } from '../utility/globalVariables';


@Injectable({
  providedIn: "root",
})
export class RolesManagementService {
  userRoles = [];

  //New Requisiitons Actions
  viewNewRequisitions = false;
  editNewRequisitions = false;
  copyNewRequisitions = false;
  approveNewRequisitions = false;
  closeNewRequisitions = false;
  editNewApprovedRequisitions = false;

  //Replace Requisition Actions
  editReplaceRequisitions = false;
  approveReplaceRequisitions = false;
  editApprovedReplaceRequisitions = false;
  closeReplaceRequisitions = false;
  assignedRequisitions = false;

  //candidate Management Actions
  viewSubmittedApplicants = false;
  editSubmittedApplicants = false;
  downloadOfferLetter = false;
  uploadApplicantsDocuments = false;
  viewApprovalsOfApplicant = false;
  sendOfferLetterToCandidate = false;
  viewSelectedCandidate = false;
  editSelectedCandidate = false;

  //Onboarding Forms Actions
  editOnboardingForms;

  //Employee Actions
  viewEmployee = false;
  viewEmployeeFinancialDetails = false;
  editEmployee = false;
  editEmployeeFinancialDetails = false;
  // 'manager','hr manager','recruiter','hr','associate','interviewer','finance','admin'
  public isAdmin = false;
  public isHrManager = false;
  public isRecruiter = false;
  public isHr = false;
  public isAssociate = false;
  public isInterviewer = false;
  public isFinance = false;
  public isManager = false;
  public isBTAdmin = false;
  roles = [];


  constructor(private globalService: GlobalVariables
  ) {
    // this.roleBasedActions();
  }

  public roleBasedActions() {
    let arr = [];
    let stored_roles = this.globalService.getRoles();
    let obj_roles = JSON.parse(stored_roles);
    if (obj_roles != undefined && obj_roles != null) {
      //let obj_roles = this.userRoles[0];
      for (let i = 0; i < obj_roles.length; i++) {
        arr.push(obj_roles[i].authority);
      }
      this.roles = arr;
    }

    // assign role based actions..
    for (let j = 0; j < arr.length; j++) {     
      if (arr[j].toLowerCase() === "admin") {
        this.viewEmployee = true;
        this.isAdmin = true;
      } else if (arr[j].toLowerCase() === "manager") {
        //New Requisiitons Actions
        this.isManager = true;
        this.viewNewRequisitions = true;
        this.editNewRequisitions = true;
        this.copyNewRequisitions = true;
        this.closeNewRequisitions = true;


        //Replace Requisition Actions
        this.editReplaceRequisitions = true;
        this.approveReplaceRequisitions = true;
        this.editApprovedReplaceRequisitions = true;
        this.closeReplaceRequisitions = true;

        //candidate Management Actions
        this.viewSubmittedApplicants = true;
        this.editSubmittedApplicants = true;
        this.downloadOfferLetter = true;
        this.uploadApplicantsDocuments = true;
        this.viewApprovalsOfApplicant = true;
        this.sendOfferLetterToCandidate = true;
        this.viewSelectedCandidate = true;
        this.editSelectedCandidate = true;

        //employee actions
        this.viewEmployee = true;
        this.viewEmployeeFinancialDetails = false;
        this.editEmployee = true;
        this.editEmployeeFinancialDetails = false;

      } else if (arr[j].toLowerCase() === "hr manager") {
        this.isHrManager = true;
        //New Requisiitons Actions
        this.viewNewRequisitions = true;
        this.editNewRequisitions = false;
        this.approveNewRequisitions = true;
        this.closeNewRequisitions = true;
        this.editNewApprovedRequisitions = false;

        //Replace Requisition Actions
        this.editReplaceRequisitions = false;
        this.approveReplaceRequisitions = true;
        this.editApprovedReplaceRequisitions = false;
        this.closeReplaceRequisitions = true;

        //candidate Management Actions
        this.viewSubmittedApplicants = true;
        this.editSubmittedApplicants = true;
        this.downloadOfferLetter = true;
        this.uploadApplicantsDocuments = true;
        this.viewApprovalsOfApplicant = true;
        this.sendOfferLetterToCandidate = true;
        this.viewSelectedCandidate = true;
        this.editSelectedCandidate = true;

        //Employee Actions
        this.viewEmployee = true;
        this.viewEmployeeFinancialDetails = true;
        this.editEmployee = true;
        this.editEmployeeFinancialDetails = true;
        


      } else if (arr[j].toLowerCase() == "recruiter") {
        this.isRecruiter = true;
        //New Requisiitons Actions
        this.assignedRequisitions = true;

        //Replace Requisition Actions

        //candidate Management Actions
        this.viewSubmittedApplicants = true;
        this.viewSelectedCandidate = true;
      } else if (arr[j].toLowerCase() == "hr") {
        this.isHr = true;
      } else if (arr[j].toLowerCase() == "associate") {
        this.isAssociate = true;
      } else if (arr[j].toLowerCase() == "interviewer") {
        this.isInterviewer = true;
      } else if (arr[j].toLowerCase() == "finance") {
        this.isFinance = true;
      }
      else if (arr[j].toLowerCase() == "btadmin") {
        this.isBTAdmin = true;
      }
    }
  }
}
