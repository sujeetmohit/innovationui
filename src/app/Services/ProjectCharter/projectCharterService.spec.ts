import { TestBed } from '@angular/core/testing';
import { ProjectCharterService } from './projectCharterService';


describe('InventoryService', () => {
  let service: ProjectCharterService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ProjectCharterService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
