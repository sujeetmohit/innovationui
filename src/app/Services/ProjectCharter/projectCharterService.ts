import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { throwError } from "rxjs";
import { catchError, retry } from "rxjs/operators";
import { api } from "../../Modals/contsants";


@Injectable({
  providedIn: 'root'
})
export class ProjectCharterService {

  baseUrl: string = api.baseURLINNOVATION;
  hrisprodUrl: string = api.hrisprodURL;

  getAllReq_url: string;
  getAll: string;
  getIsUsed: string;


  constructor(
    private httpClient: HttpClient,
  ) { }


  public getResultAreaType() {
    return this.httpClient
      .get<any>(this.baseUrl + "common/getResultAreaType")
      .pipe(retry(1), catchError(this.handleError));
  }

  public getDMAICPhaseToll() {
    return this.httpClient
      .get<any>(this.baseUrl + "common/getDMAICPhase")
      .pipe(retry(1), catchError(this.handleError));
  }

  public getProjectCharterDetails() {
    this.getAllReq_url = this.baseUrl + "projectCharter/GetProjectCharterDetails";
    return this.httpClient
      .get(this.getAllReq_url, {
        headers: new HttpHeaders({
          "Content-Type": "application/json",
        }),
      })
      .pipe(retry(1), catchError(this.handleError));
  }

  public getProjectCharterByProjectIdDetails(projectId) {
    this.getAllReq_url = this.baseUrl + "projectCharter/GetProjectCharterByProjectIdDetails?projectId=" + projectId;
    return this.httpClient
      .get(this.getAllReq_url, {
        headers: new HttpHeaders({
          "Content-Type": "application/json",
        }),
      })
      .pipe(retry(1), catchError(this.handleError));
  }

  public getProjectCharterResultAreaDetails(projectId, projectTypeId) {
    this.getAllReq_url = this.baseUrl + "projectCharter/GetProjectCharterResultArea?p_ProjectId=" + projectId +
      "&p_ProjectTypeId=" + projectTypeId;
    return this.httpClient
      .get(this.getAllReq_url, {
        headers: new HttpHeaders({
          "Content-Type": "application/json",
        }),
      })
      .pipe(retry(1), catchError(this.handleError));
  }

  public getProjectSteeringCommiteeDetails(projectId, projectTypeId) {
    this.getAllReq_url = this.baseUrl + "projectCharter/GetProjectCharterSteeringCommitte?p_ProjectId=" + projectId +
      "&p_ProjectTypeId=" + projectTypeId;
    return this.httpClient
      .get(this.getAllReq_url, {
        headers: new HttpHeaders({
          "Content-Type": "application/json",
        }),
      })
      .pipe(retry(1), catchError(this.handleError));
  }

  public getProjectCharterBenefitsDetails(projectId, projectTypeId) {

    this.getAllReq_url = this.baseUrl + "projectCharter/GetProjectCharterBenefits?p_ProjectId=" + projectId +
      "&p_ProjectTypeId=" + projectTypeId;
    return this.httpClient
      .get(this.getAllReq_url, {
        headers: new HttpHeaders({
          "Content-Type": "application/json",
        }),
      })
      .pipe(retry(1), catchError(this.handleError));
  }

  public getProjectCharterDMAICDetails(projectId, projectTypeId) {

    this.getAllReq_url = this.baseUrl + "projectCharter/GetProjectCharterDMAICPhase?p_ProjectId=" + projectId +
      "&p_ProjectTypeId=" + projectTypeId;
    return this.httpClient
      .get(this.getAllReq_url, {
        headers: new HttpHeaders({
          "Content-Type": "application/json",
        }),
      })
      .pipe(retry(1), catchError(this.handleError));
  }

  public getProjectCharterSteeringTeamDetails(projectId, projectTypeId) {

    this.getAllReq_url = this.baseUrl + "projectCharter/GetProjectCharterSteeringComtProjectTeam?p_ProjectId=" + projectId +
      "&p_ProjectTypeId=" + projectTypeId;
    return this.httpClient
      .get(this.getAllReq_url, {
        headers: new HttpHeaders({
          "Content-Type": "application/json",
        }),
      })
      .pipe(retry(1), catchError(this.handleError));
  }

  public getProjectCharterKeyStakeHoldersDetails(projectId, projectTypeId) {

    this.getAllReq_url = this.baseUrl + "projectCharter/GetProjectCharterKeyStakeHolders?p_ProjectId=" + projectId +
      "&p_ProjectTypeId=" + projectTypeId;
    return this.httpClient
      .get(this.getAllReq_url, {
        headers: new HttpHeaders({
          "Content-Type": "application/json",
        }),
      })
      .pipe(retry(1), catchError(this.handleError));
  }

  public getProjectCharterDocumentsDetails(projectId, projectTypeId) {

    this.getAllReq_url = this.baseUrl + "projectCharter/GetProjectCharterDocuments?p_ProjectId=" + projectId +
      "&p_ProjectTypeId=" + projectTypeId;
    return this.httpClient
      .get(this.getAllReq_url, {
        headers: new HttpHeaders({
          "Content-Type": "application/json",
        }),
      })
      .pipe(retry(1), catchError(this.handleError));
  }

  public getProjectCharterRevisionHistoryDetails(projectId, projectTypeId) {
    this.getAllReq_url = this.baseUrl + "projectCharter/GetProjectCharterRivisionHistory?p_ProjectId=" + projectId +
      "&p_ProjectTypeId=" + projectTypeId;
    return this.httpClient
      .get(this.getAllReq_url, {
        headers: new HttpHeaders({
          "Content-Type": "application/json",
        }),
      })
      .pipe(retry(1), catchError(this.handleError));
  }

  public InsertProjectCharterDetails(projectCharter) {
  
    return this.httpClient.post(this.baseUrl + 'projectCharter/insertProjectCharterdetail', projectCharter, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      })
    }).pipe(
      retry(1),
      catchError(this.handleError)
    );
  }

  public UpdateProjectCharterStatus(projectId, updatedBy, comments, projectStatusId) {
    this.getAllReq_url = this.baseUrl + "projectCharter/updateProjectCharterStatus?projectId=" + projectId +
      "&updatedBy=" + updatedBy + "&comments=" + comments + "&projectStatusId=" + projectStatusId;
    return this.httpClient
      .post(this.getAllReq_url, {
        headers: new HttpHeaders({
          "Content-Type": "application/json",
        }),
      })
      .pipe(retry(1), catchError(this.handleError));
  }

  public UpdateProjectCharterDetails(projectCharter) {
    return this.httpClient.post(this.baseUrl + 'projectCharter/updateProjectCharterdetail', projectCharter, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      })
    }).pipe(
      retry(1),
      catchError(this.handleError)
    );
  }

  public InsertProjectCharterResultArea(blankResultAreaArray: any, projectId: number, projectTypeId: number) {
    return this.httpClient.post(this.baseUrl + "projectCharter/insertProjectCharterResultAreaDetail?projectId=" + projectId + "&projectTypeId=" + projectTypeId, blankResultAreaArray, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      })
    }).pipe(
      retry(1),
      catchError(this.handleError)
    );
  }

  public InsertProjectCharterDmaicphaseToll(dmailTollPhaseArray: any, projectId: number, projectTypeId: number) {
    return this.httpClient.post(this.baseUrl + "projectCharter/insertProjectCharterDmaicphaseToll?projectId=" + projectId + "&projectTypeId=" + projectTypeId, dmailTollPhaseArray, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      })
    }).pipe(
      retry(1),
      catchError(this.handleError)
    );
  }

  public InsertProjectCharterSteeringCommittedetail(projectCharterSteeringCommiteeObj: any, projectId: number, projectTypeId: number) {
    
    return this.httpClient.post(this.baseUrl + "projectCharter/insertProjectCharterSteeringCommittedetail?projectId=" + projectId + "&projectTypeId=" + projectTypeId, projectCharterSteeringCommiteeObj, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      })
    }).pipe(
      retry(1),
      catchError(this.handleError)
    );
  }

  public InsertProjectCharterSteeringCommitteTeamdetail(steeringcomiteeTeamArray: any, projectId: number, projectTypeId: number) {
    return this.httpClient.post(this.baseUrl + "projectCharter/insertProjectCharterSteeringCommitteTeamdetail?projectId=" + projectId + "&projectTypeId=" + projectTypeId, steeringcomiteeTeamArray, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      })
    }).pipe(
      retry(1),
      catchError(this.handleError)
    );
  }

  public InsertProjectCharterBenefitDetail(blankBenefitsArray: any, projectId: number, projectTypeId: number) {
    return this.httpClient.post(this.baseUrl + "projectCharter/insertProjectCharterBenefitDetail?projectId=" + projectId + "&projectTypeId=" + projectTypeId, blankBenefitsArray, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      })
    }).pipe(
      retry(1),
      catchError(this.handleError)
    );
  }

  public InsertProjectCharterKeyStakeHolderdetail(keyStakeHoldersArray: any, projectId: number, projectTypeId: number) {
    return this.httpClient.post(this.baseUrl + "projectCharter/insertProjectCharterKeyStakeHolderdetail?projectId=" + projectId + "&projectTypeId=" + projectTypeId, keyStakeHoldersArray, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      })
    }).pipe(
      retry(1),
      catchError(this.handleError)
    );
  }

  public InsertProjectCharterDocumentDetail(documentsUpdatedArray: any, projectId: number, projectTypeId: number) {
    return this.httpClient.post(this.baseUrl + "projectCharter/insertProjectCharterDocumentDetail?projectId=" + projectId + "&projectTypeId=" + projectTypeId, documentsUpdatedArray, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      })
    }).pipe(
      retry(1),
      catchError(this.handleError)
    );
  }

  public InsertProjectCharterRevisionHistorydetail(revisionHistoryArray: any, projectId: number, projectTypeId: number) {
    return this.httpClient.post(this.baseUrl + "projectCharter/insertProjectCharterRevisionHistorydetail?projectId=" + projectId + "&projectTypeId=" + projectTypeId, revisionHistoryArray, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      })
    }).pipe(
      retry(1),
      catchError(this.handleError)
    );
  }


  //handle http errors
  handleError(error) {
    let errorMessage = "";
    if (error.error instanceof ErrorEvent) {
      // client-side error
      errorMessage = `Error: ${error.error.message}`;
    } else {
      // server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }

    return throwError(errorMessage);
  }

}
