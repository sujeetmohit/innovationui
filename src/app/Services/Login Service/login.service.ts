import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { retry, catchError } from "rxjs/operators";
import { throwError } from "rxjs/internal/observable/throwError";
import { api } from '../../Modals/contsants';

@Injectable({
  providedIn: "root",
})
export class LoginService {
  rolesList = new Array();
  successLogin: boolean = true;

  constructor(private httpClient: HttpClient) { }

  baseUrl: string = api.baseURL;

  getAllReq_url: string;
  getById: string;

  hrisLoginUrl: string;

  // Login request with username and passwor..
  public loginRequest(obj) {
    this.hrisLoginUrl = 'http://10.1.10.138:8080/AISHRMIS-0.0.1-SNAPSHOT/api/'

    return this.httpClient.post(this.hrisLoginUrl + "auth/authenticate", obj, {
      headers: new HttpHeaders({
        "Content-Type": "application/json",
      }),
    }).pipe(catchError(this.handleError));
  }

  // public TestoginRequest(obj) {
    

  //   debugger;

  //   return this.httpClient.post('http://10.1.10.138:8080/SSOLogin/api/auth/authenticate',obj, {
  //     headers: new HttpHeaders({
  //       "Content-Type": "application/json",
  //     }),
  //   }).pipe(catchError(this.handleError));
  // }


  handleError(error) {
    this.successLogin = false;

    let errorMessage = "";
    if (error.error instanceof ErrorEvent) {
      // client-side error
      errorMessage = `Error: ${error.error.message}`;
    } else {
      // server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }

    return throwError(errorMessage);
  }
}
