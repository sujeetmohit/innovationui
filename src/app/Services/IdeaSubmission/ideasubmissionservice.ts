import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { throwError } from "rxjs";
import { catchError, retry } from "rxjs/operators";
import { api } from "../../Modals/contsants";


@Injectable({
  providedIn: 'root'
})
export class IdeaSubmission {

  baseUrl: string = api.baseURLINNOVATION;
  hrisprodUrl: string = api.hrisprodURL;

  getAllReq_url: string;
  getAll: string;
  getIsUsed: string;


  constructor(
    private httpClient: HttpClient,
  ) { }


  public getIdeaSubmissionDetails(loginEmailId) {
    this.getAllReq_url = this.baseUrl + "ideaSubmission/getIdeaSubmissionDetails?loginEmailId=" + loginEmailId;
    return this.httpClient
      .get(this.getAllReq_url, {
        headers: new HttpHeaders({
          "Content-Type": "application/json",
        }),
      })
      .pipe(retry(1), catchError(this.handleError));
  }

  public getIdeaSubmissionbyId(ideaSubmissionId) {
    this.getAllReq_url = this.baseUrl + "ideaSubmission/getIdeaSubmissionbyId?ideaSubmissionId=" + ideaSubmissionId;
    return this.httpClient
      .get(this.getAllReq_url, {
        headers: new HttpHeaders({
          "Content-Type": "application/json",
        }),
      })
      .pipe(retry(1), catchError(this.handleError));
  }

  public getApprovedIdeaSubmission(loginEmailId, managerStatusId) {
    this.getAllReq_url = this.baseUrl + "ideaSubmission/getApprovedIdeaSubmissionDetails?loginEmailId=" + loginEmailId + '&managerStatusId=' + managerStatusId;
    return this.httpClient
      .get(this.getAllReq_url, {
        headers: new HttpHeaders({
          "Content-Type": "application/json",
        }),
      })
      .pipe(retry(1), catchError(this.handleError));
  }

  public updateManagerStatus(ideaSubmissionId, managerStatusId, managerFeedback) {
    this.getAllReq_url = this.baseUrl + "ideaSubmission/updateManagerStatus?ideaSubmissionId="
      + ideaSubmissionId + '&managerStatusId=' + managerStatusId + '&managerFeedback=' + managerFeedback;
    return this.httpClient
      .post(this.getAllReq_url, {
        headers: new HttpHeaders({
          "Content-Type": "application/json",
        }),
      })
      .pipe(retry(1), catchError(this.handleError));
  }

  //fetch Lever Technology
  public getLeverTechnologyDetails() {
    return this.httpClient
      .get<any>(this.baseUrl + "common/getLeverTechnology")
      .pipe(retry(1), catchError(this.handleError));
  }

  //fetch Project Type
  public getProjectTypeDetails() {
    return this.httpClient
      .get<any>(this.baseUrl + "common/getProjectType")
      .pipe(retry(1), catchError(this.handleError));
  }

  public uploadIdeaSubmissionFile(file: File, ideaSubmissionId: Number) {
    this.getAllReq_url = this.baseUrl + "fileupload/ideaSubmission/uploadIdeaSubmission?ideaSubmissionId=" + ideaSubmissionId;

    const formData: FormData = new FormData();
    formData.append('ideaSubmissionfile', file);

    return this.httpClient
      .post(this.getAllReq_url, formData)
      .pipe(retry(2), catchError(this.handleError));
  }

  public deleteIdeaSubmissionFile(ideaSubmissionId: number) {
    let url = this.baseUrl + "fileupload/ideaSubmission/deleteIdeaSubmission?ideaSubmissionId=" + ideaSubmissionId;
    return this.httpClient
      .get(url, {
        headers: new HttpHeaders({
          "Content-Type": "application/json",
        }),
      })
      .pipe(retry(1), catchError(this.handleError));
  }

  //Insert Idea Submission..
  public InsertUpdateIdeaSubmissionDetails(ideaSubmissionObj) {
    return this.httpClient.post(this.baseUrl + 'ideaSubmission/insertIdeaSubmissiondetail', ideaSubmissionObj, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      })
    }).pipe(
      retry(1),
      catchError(this.handleError)
    );
  }

  public getEmployeeDetailsByEmail(loginEmailId) {
    this.getAllReq_url = this.hrisprodUrl + "employee/getEmployeebyOfficialEmail?officialEmailId=" + loginEmailId;
    return this.httpClient
      .get(this.getAllReq_url, {
        headers: new HttpHeaders({
          "Content-Type": "application/json",
        }),
      })
      .pipe(retry(1), catchError(this.handleError));
  }

 
  //Update Idea Submission..
  public UpdateIdeaSubmissionDetails(ideaSubmissionObj) {
    return this.httpClient.post(this.baseUrl + 'ideaSubmission/updateIdeaSubmissiondetail', ideaSubmissionObj, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      })
    }).pipe(
      retry(1),
      catchError(this.handleError)
    );
  }


  //handle http errors
  handleError(error) {
    let errorMessage = "";
    if (error.error instanceof ErrorEvent) {
      // client-side error
      errorMessage = `Error: ${error.error.message}`;
    } else {
      // server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }

    return throwError(errorMessage);
  }

}
