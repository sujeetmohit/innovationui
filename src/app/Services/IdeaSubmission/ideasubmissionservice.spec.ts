import { TestBed } from '@angular/core/testing';
import { IdeaSubmission } from './ideasubmissionservice';


describe('InventoryService', () => {
  let service: IdeaSubmission;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(IdeaSubmission);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
