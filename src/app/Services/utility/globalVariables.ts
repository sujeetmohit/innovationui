import { Injectable } from '@angular/core';



@Injectable()
export class GlobalVariables {

  private paramsData: any;
  private paramData: any;
  private retainParamValue: boolean = false;
  private userType: string = "";
  private phraseToShow: string = "";
  private user: any;
  private uid: any;
  private userData: any;
  private userEmail: string;
  private empId: string;
  private roles: any;
  private token: string;

  private userName: string;

  constructor() {
  }

  public getParameterData() {
    const output = this.paramsData;
    if (this.retainParamValue === false) {
      this.setParameterData(null);
    }
    return output;
  }

  public setParameterData(input, retainValue: boolean = false) {
    this.paramsData = input;
    this.retainParamValue = retainValue;
  }

  public setPhraseToShow(input) {
    this.phraseToShow = input;
  }

  public getPhraseToShow() {
    const output = this.phraseToShow;
    this.setPhraseToShow(" ");
    return output;
  }

  setUid(uid: any) {
    this.uid = uid;
  }

  getUid() {
    return this.uid;
  }

  // showLoading() {
  //   this.loader.open();
  // }

  // hideLoading() {
  //   this.loader.close();
  // }

  public setUserType(userType: string) {
    this.userType = userType;
  }

  public getUserType() {
    return this.userType;
  }

  public setUser(user: any) {
    this.user = user;
  }

  public getUser() {
    return this.user;
  }

  public setParamData(input) {
    this.paramData = input;
  }

  public getParamData() {
    return this.paramData;
  }

  public setUserKey(data: any) {
    this.userData = data;
  }

  public getUserKey() {
    return this.userData;
  }

  public setUserEmail(email: string) {
    this.userEmail = email;
  }

  public getUserEmail() {
    return this.userEmail;
  }

  public setUserName(userName: string) {
    this.userName = userName;
  }

  public getUserName() {
    return this.userName;
  }


  public setEmpId(empId: string) {
    this.empId = empId;
  }

  public getEmpId() {
    return this.empId;
  }

  public setRoles(roles: any) {
    this.roles = roles;
  }

  public getRoles() {
    return this.roles;
  }

  public setToken(token: string) {
    this.token = token;
  }

  public getToken() {
    return this.token;
  }

}
